# Module Simulator
Simulator of accelerating cavities

## Objectives

### CW operations

*   Make a CW open-loop code in baseband
*   Introduce noise in the model
*   Determine the performance in closed loop as a function of:
    1. Dw(t): cryo microphonics
    2. Q_l: bandwidth of cavities
    3. Vector sum: number of controlled cavities
    4. Ibeam: beam loading

### Pulsed Mode

* execute split cavity tests. TODO: fill this section with data 

## Information of the system.

Informations about the system:

- R/Q = 1040
- E_{acc} = 5 - 20 MV/m: This Value should be optimized to be maximized
- N_cavs = 1,2,8,16
- k_{LFD} = 0.1 - 0.5 ask Radek
- QL: range of 2 * 10^7
- Microphonics: sinuisoidal disturbance on Dw of with an upper limit of 100 Hz. ask Radek for experimental result.
    Also reasonable amplitudes for these disturbances should be decided.
- Ibeam: order of 0.5 nC per bunch, order of 4us separation, bunch length 80 fs
- Vector sum: loop bandwidth of 1.5 us.. See the response of the LLRF controller?

## Work to be done

- Adapt The Cavity Module code to have an initial cavity status value.

- Determine how the LLRF should behave: could it be a flat response system with shifting properties?
    Will it have an integration behaviour? (PI?) or even more complicate? (PID)? What about controlling the phase
    Experts must be asked for more informations.. S.Pfeiffer and C.Schmidt PhD Theses could be of great help.
- Perform closed loop simulations without Ibeam with reasonable set of parameters
    A possibility could be to make graphs of the peak-peak amplitude of microphonics in function of the 
    loaded Q of the cavities at discrete E_acc

- Perform the same simulations with beam loading. Start with default parameter


The simulation length should be decided. 1s could be a reasonable time.

## Requirements

- Boost.Python : http://www.boost.org/doc/libs/1_65_0/libs/python/doc/html/index.html
- JsonCPP : https://github.com/open-source-parsers/jsoncpp
- Python3 development libraries : https://www.python.org

Note that only __Python3__ is supported!!
