from distutils.core import setup, Extension
from glob import glob
import sys


if (sys.version_info < (3, 0)):
    py_lib = 'python2.7'
    boost_py_lib = 'boost_python-py27'
else:
    py_lib = 'python3.5m'
    boost_py_lib = 'boost_python-py35'

module_simulator = Extension('module_simulator.core',
        define_macros = [('MODULE_NAME', 'module_simulator')],
        language='c++',
        libraries = ['jsoncpp', py_lib, boost_py_lib, 'pthread'],
        extra_link_args = ['-flto', '-fopenmp'],
        extra_compile_args = ['-Ofast', '-flto', '-fopenmp', '-fno-use-linker-plugin', '-march=native',
                              '-pthread', '-std=c++11'],
        sources = glob('./src/*.cpp'))

setup (name = 'ModuleSimulator',
       version = '1.0',
       description = 'Cavity modules simulator',
       author = 'Andrea Bellandi',
       author_email = 'andrea.bellandi@desy.de',
       url = 'https://github.com/Bellaz/ModuleSimulator',
       long_description = '''
        Module Simulator provides an high speed library to study SRF accelerating cavities modules
       ''',
       packages = ['module_simulator', 'module_simulator.envelope'],
       package_dir = {'module_simulator' : 'python', 
                      'module_simulator.envelope' : 'python/envelope'},
       ext_modules = [module_simulator])
