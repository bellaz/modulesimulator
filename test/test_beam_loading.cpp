#include<gtest/gtest.h>
#include<cmath>
#include<cstdlib>
#include<string>
#include "beam_loading.h"

#define _USE_MATH_DEFINES
#define NUM_TEST_FMOD 100000
#define MAX_VALUES 100000.000

#define TIMESTEP 0.01
#define BASEBAND_FREQUENCY 1.0
#define BUNCH_CHARGE 1.0
#define BUNCH_SEPARATION 7.0
#define BUNCH_PHASE 0//(0.1*2.0*M_PI)
#define START_TIME 100.0
#define N_BUNCHES 270
#define STOP_TIME 5000.0
#define BASEBAND_PHASE 0 //(0.2*2.0*M_PI)

using namespace std;

TEST(TestBeamLoading, Assertmfmod) {

    for(unsigned int i = 0; i < NUM_TEST_FMOD; i++){

        double num = ((double)rand() / RAND_MAX) * MAX_VALUES;
        double den = ((double)(abs(rand())+1.0) / RAND_MAX) * MAX_VALUES;
        ASSERT_FLOAT_EQ(mfmod(num, den), fmod(num, den));
    }
};

TEST(TestBeamLoading, AssertEq){

    BeamLoading b1(TIMESTEP,
            BASEBAND_FREQUENCY,
            BUNCH_CHARGE,
            BUNCH_SEPARATION,
            BUNCH_PHASE,
            START_TIME,
            N_BUNCHES,
            BASEBAND_PHASE);

    BeamLoading b2(TIMESTEP,
            BASEBAND_FREQUENCY,
            BUNCH_CHARGE,
            BUNCH_SEPARATION,
            BUNCH_PHASE,
            START_TIME,
            N_BUNCHES,
            BASEBAND_PHASE);

    ASSERT_EQ(b1, b2);
}

TEST(TestBeamLoading, AssertFiniteTimeBL){

    BeamLoading bl(TIMESTEP,
            BASEBAND_FREQUENCY,
            BUNCH_CHARGE,
            BUNCH_SEPARATION,
            BUNCH_PHASE,
            START_TIME,
            N_BUNCHES,
            BASEBAND_PHASE);
  
    unsigned long int steps_before_bl = START_TIME / TIMESTEP -1;
    steps_before_bl += (BUNCH_PHASE + BASEBAND_PHASE)/(2.0 * M_PI * BASEBAND_FREQUENCY * TIMESTEP);
    unsigned long int steps_between_pulses = BUNCH_SEPARATION / (BASEBAND_FREQUENCY * TIMESTEP);
    unsigned long int total_steps = STOP_TIME / TIMESTEP; 
    double bl_current =  BUNCH_CHARGE / TIMESTEP;
    unsigned int pulse_done = 0;
    unsigned long int i = 0;


    for(; i < steps_before_bl; i++){

    bl.step();
    EXPECT_FLOAT_EQ(bl.get_current(), 0.0) << " at step " << i;
    }
    
    for(; i < steps_before_bl + steps_between_pulses * N_BUNCHES; i++){

        bl.step();
        if (((i - steps_before_bl) % steps_between_pulses) == 0) {
            
            pulse_done++;
            EXPECT_FLOAT_EQ(bl.get_current(), bl_current)<< " at step " << i;
        } else {
            EXPECT_FLOAT_EQ(bl.get_current(), 0.0)<< " at step " << i;
        }
    }
    
    ASSERT_EQ(pulse_done, N_BUNCHES);
    
    for(; i < total_steps; i++){

    bl.step();
    ASSERT_FLOAT_EQ(bl.get_current(), 0.0)<< " at step " << i;
    }

}

TEST(TestBeamLoading, AssertInfiniteTimeBL){

    BeamLoading bl(TIMESTEP,
            BASEBAND_FREQUENCY,
            BUNCH_CHARGE,
            BUNCH_SEPARATION,
            BUNCH_PHASE,
            START_TIME,
            -1,
            BASEBAND_PHASE);
  
    unsigned long int steps_before_bl = START_TIME / TIMESTEP -1;
    steps_before_bl += (BUNCH_PHASE + BASEBAND_PHASE)/(2.0 * M_PI * BASEBAND_FREQUENCY * TIMESTEP);
    unsigned long int steps_between_pulses = BUNCH_SEPARATION / (BASEBAND_FREQUENCY * TIMESTEP);
    unsigned long int total_steps = STOP_TIME / TIMESTEP; 
    double bl_current =  BUNCH_CHARGE / TIMESTEP;
    unsigned long int i = 0;


    for(; i < steps_before_bl; i++){

    bl.step();
    EXPECT_FLOAT_EQ(bl.get_current(), 0.0) << " at step " << i;
    }
    
    for(; i < total_steps; i++){

        bl.step();
        if (((i - steps_before_bl) % steps_between_pulses) == 0) {
            
            EXPECT_FLOAT_EQ(bl.get_current(), bl_current)<< " at step " << i;
        } else {
            EXPECT_FLOAT_EQ(bl.get_current(), 0.0)<< " at step " << i;
        }
    }
    
}


TEST(TestBeamLoading, AssertReset){

    BeamLoading bl(TIMESTEP,
            BASEBAND_FREQUENCY,
            BUNCH_CHARGE,
            BUNCH_SEPARATION,
            BUNCH_PHASE,
            START_TIME,
            -1,
            BASEBAND_PHASE);
  
    unsigned long int steps_before_bl = START_TIME / TIMESTEP -1;
    steps_before_bl += (BUNCH_PHASE + BASEBAND_PHASE)/(2.0 * M_PI * BASEBAND_FREQUENCY * TIMESTEP);
    unsigned long int steps_between_pulses = BUNCH_SEPARATION / (BASEBAND_FREQUENCY * TIMESTEP);
    unsigned long int total_steps = STOP_TIME / TIMESTEP; 
    double bl_current =  BUNCH_CHARGE / TIMESTEP;
    unsigned long int i = 0;


    for(; i < steps_before_bl; i++){

    bl.step();
    EXPECT_FLOAT_EQ(bl.get_current(), 0.0) << " at step " << i;
    }
    
    for(; i < total_steps; i++){

        bl.step();
        if (((i - steps_before_bl) % steps_between_pulses) == 0) {
            
            EXPECT_FLOAT_EQ(bl.get_current(), bl_current)<< " at step " << i;
        } else {
            EXPECT_FLOAT_EQ(bl.get_current(), 0.0)<< " at step " << i;
        }
    }

    bl.reset();
    i = 0;

    for(; i < steps_before_bl; i++){

    bl.step();
    EXPECT_FLOAT_EQ(bl.get_current(), 0.0) << " at step " << i;
    }
    
    for(; i < total_steps; i++){

        bl.step();
        if (((i - steps_before_bl) % steps_between_pulses) == 0) {
            
            EXPECT_FLOAT_EQ(bl.get_current(), bl_current)<< " at step " << i;
        } else {
            EXPECT_FLOAT_EQ(bl.get_current(), 0.0)<< " at step " << i;
        }
    }


}



TEST(TestBeamLoading, AssertSerializeDeserialize){

    BeamLoading b1(TIMESTEP,
            BASEBAND_FREQUENCY,
            BUNCH_CHARGE,
            BUNCH_SEPARATION,
            BUNCH_PHASE,
            START_TIME,
            N_BUNCHES,
            BASEBAND_PHASE);

    Json::Value serialized_bl = b1.serialize();
    BeamLoading b2 = BeamLoading::deserialize(serialized_bl);

    ASSERT_EQ(b1, b2);

}

TEST(TestBeamLoading, AssertNoArgs){

    BeamLoading();
}
