/*
 * Test module for IdealCavity
 */

#include<gtest/gtest.h>
#include<cmath>
#include<string>
#include "ideal_cavity.h"
#include "discrete_oscillator.h"

#define _USE_MATH_DEFINES
#define TIMESTEP 0.001
#define FREQUENCY 1.00
#define QV 234.0
#define ROQ (1.0/QV)
#define OSC1_FREQ FREQUENCY
#define DELTA_FREQUENCY (0.5*FREQUENCY/QV)
#define OSC2_FREQ (FREQUENCY+DELTA_FREQUENCY)
#define N_QS 8
#define DELTA_GRADIENT (1.01/QV)
#define DELTA_ANGLE (2.02*M_PI/QV)
#define ANG 0.33
#define ARB1 0.208
#define ARB2 0.254

using namespace std;

class TestIdealCavity : public ::testing::Test {
    protected:
        virtual void SetUp() {

            cav1 = IdealCavity(TIMESTEP, FREQUENCY, QV, ROQ);
            cav2 = IdealCavity(TIMESTEP, FREQUENCY, QV, ROQ);
            osc1 = DiscreteOscillator(TIMESTEP, OSC1_FREQ);
            osc2 = DiscreteOscillator(TIMESTEP, OSC2_FREQ);   

            for(int i = 0; i < (int)(QV * N_QS/(TIMESTEP*FREQUENCY)); i++){
                cav1.step(osc1.get_cos()); 
                cav2.step(osc2.get_cos());
                osc1.advance();
                osc2.advance();
            } 
            cav1.step(osc1.get_cos());
            cav2.step(osc1.get_cos());

        }

        virtual void TearDown() {

        }

        IdealCavity cav1;
        IdealCavity cav2;
        DiscreteOscillator osc1;
        DiscreteOscillator osc2;
};

TEST_F(TestIdealCavity, AssertSerializeDeserialize){

    Json::Value serialized_cavity = cav1.serialize();
    IdealCavity cav3 = IdealCavity::deserialize(serialized_cavity);
    ASSERT_EQ(cav1, cav3);
};

TEST_F(TestIdealCavity, AssertGetAngle){

    IQValue state(cos(ANG), sin(ANG)/(2.0 * M_PI * FREQUENCY));    
    IdealCavity cav(TIMESTEP, FREQUENCY, QV, ROQ, state);
    ASSERT_FLOAT_EQ(cav.get_angle(FREQUENCY), ANG);

};

TEST_F(TestIdealCavity, AssertGetAngleTime){
    
    IQValue state(cos(ANG), sin(ANG)/(2.0 * M_PI * FREQUENCY));    
    IdealCavity cav(TIMESTEP, FREQUENCY, QV, ROQ, state);
    ASSERT_FLOAT_EQ(cav.get_angle_as_time(FREQUENCY), ANG/(2.0 * M_PI * FREQUENCY));
};

TEST_F(TestIdealCavity, AssertReachedGradientAndPhaseTuned){

    ASSERT_LT(cav1.get_angle(OSC1_FREQ), osc1.get_angle() + DELTA_ANGLE);
    ASSERT_GT(cav1.get_angle(OSC1_FREQ), osc1.get_angle() - DELTA_ANGLE);
    ASSERT_LT(cav1.get_actual_V(), 1.0 + DELTA_GRADIENT);
    ASSERT_GT(cav1.get_actual_V(), 1.0 - DELTA_GRADIENT);
};

TEST_F(TestIdealCavity, AssertReachedGradientAndPhaseDetuned){

    double tanphi = -2.0 * QV * DELTA_FREQUENCY / OSC2_FREQ;
    ASSERT_LT(cav2.get_angle(OSC2_FREQ), osc2.get_angle() + atan(tanphi) + DELTA_ANGLE);
    ASSERT_GT(cav2.get_angle(OSC2_FREQ), osc2.get_angle() + atan(tanphi) - DELTA_ANGLE);
    ASSERT_LT(cav2.get_actual_V(), cos(cav2.get_angle(OSC2_FREQ))/sqrt(1.0 + pow(tanphi, 2.0)) + DELTA_GRADIENT);
    ASSERT_GT(cav2.get_actual_V(), cos(cav2.get_angle(OSC2_FREQ))/sqrt(1.0 + pow(tanphi, 2.0)) - DELTA_GRADIENT);

};

TEST_F(TestIdealCavity, AssertAddAngle){

    double timephase1 = cav1.get_angle(OSC1_FREQ);
    double timephase2 = cav2.get_angle(OSC2_FREQ);
    cav1.add_angle(OSC1_FREQ, ARB1);
    cav2.add_angle(OSC2_FREQ, ARB2);
    ASSERT_LT(cav1.get_angle(OSC1_FREQ), timephase1 + ARB1 + DELTA_ANGLE);
    ASSERT_GT(cav1.get_angle(OSC1_FREQ), timephase1 + ARB1 - DELTA_ANGLE);
    ASSERT_LT(cav2.get_angle(OSC2_FREQ), timephase2 + ARB2 + DELTA_ANGLE);
    ASSERT_GT(cav2.get_angle(OSC2_FREQ), timephase2 + ARB2 - DELTA_ANGLE);
}

TEST_F(TestIdealCavity, AssertAddAngleTime){

    double timephase1 = cav1.get_angle_as_time(OSC1_FREQ);
    double timephase2 = cav2.get_angle_as_time(OSC2_FREQ);
    cav1.add_angle_as_time(OSC1_FREQ, ARB1);
    cav2.add_angle_as_time(OSC2_FREQ, ARB2);
    ASSERT_LT(cav1.get_angle_as_time(OSC1_FREQ), timephase1 + ARB1 + DELTA_ANGLE);
    ASSERT_GT(cav1.get_angle_as_time(OSC1_FREQ), timephase1 + ARB1 - DELTA_ANGLE);
    ASSERT_LT(cav2.get_angle_as_time(OSC2_FREQ), timephase2 + ARB2 + DELTA_ANGLE);
    ASSERT_GT(cav2.get_angle_as_time(OSC2_FREQ), timephase2 + ARB2 - DELTA_ANGLE);
}

TEST_F(TestIdealCavity, AssertSetAngle){

    cav1.set_angle(OSC1_FREQ, ARB1);
    EXPECT_FLOAT_EQ(cav1.get_angle(OSC1_FREQ), ARB1);
}

TEST_F(TestIdealCavity, AssertSetAngleTime){

    cav1.set_angle_as_time(OSC1_FREQ, ARB1);
    EXPECT_FLOAT_EQ(cav1.get_angle_as_time(OSC1_FREQ), ARB1);
}

