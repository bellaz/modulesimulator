/*
 * Test module for DiscreteOscillator
 */

#include<gtest/gtest.h>
#include<cmath>
#include<string>
#include "discrete_oscillator.h"

#define _USE_MATH_DEFINES

#define TIME_CICLE 123

#define TIMESTEP1 0.0100
#define FREQUENCY1 0.13
#define PHASE1 M_PI/7.0
#define AMP 3.3

#define TIMESTEP2 0.0025
#define FREQUENCY2 0.14
#define PHASE2 M_PI/5.0
#define ARB1 1.908
#define ARB2 0.754


using namespace std;

class TestDiscreteOscillator : public ::testing::Test {
    protected:
        virtual void SetUp() {

            do1 = DiscreteOscillator(TIMESTEP1, FREQUENCY1, PHASE1, AMP);
            do2 = DiscreteOscillator(TIMESTEP2, FREQUENCY2, PHASE2, AMP);
        }

        virtual void TearDown() {

        }

    DiscreteOscillator do1;
    DiscreteOscillator do2;
};

TEST_F(TestDiscreteOscillator, AssertGetCosSin){
    double cosp, sinp;
    do1.get_cos_sin(cosp, sinp);
    EXPECT_FLOAT_EQ(cosp, cos(PHASE1) * AMP);
    EXPECT_FLOAT_EQ(sinp, sin(PHASE1) * AMP);
    do2.get_cos_sin(cosp, sinp);
    EXPECT_FLOAT_EQ(cosp, cos(PHASE2) * AMP);
    EXPECT_FLOAT_EQ(sinp, sin(PHASE2) * AMP);
    EXPECT_FLOAT_EQ(cosp, do2.get_cos());
    EXPECT_FLOAT_EQ(sinp, do2.get_sin());
}

TEST_F(TestDiscreteOscillator, AssertAngle){
    
    EXPECT_FLOAT_EQ(do1.get_angle(), PHASE1);
    EXPECT_FLOAT_EQ(do2.get_angle(), PHASE2);
}


TEST_F(TestDiscreteOscillator, AssertAngleTime){
    
    EXPECT_FLOAT_EQ(do1.get_angle_as_time(), PHASE1/(2.0 * M_PI * FREQUENCY1));
    EXPECT_FLOAT_EQ(do2.get_angle_as_time(), PHASE2/(2.0 * M_PI * FREQUENCY2));
}

TEST_F(TestDiscreteOscillator, AssertAddAngle){

    do1.add_angle(ARB1);
    do2.add_angle(ARB2);
    EXPECT_FLOAT_EQ(do1.get_angle(), PHASE1 + ARB1);
    EXPECT_FLOAT_EQ(do2.get_angle(), PHASE2 + ARB2);
}

TEST_F(TestDiscreteOscillator, AssertAddAngleTime){

    double timephase1 = do1.get_angle_as_time();
    double timephase2 = do2.get_angle_as_time();
    do1.add_angle_as_time(ARB1);
    do2.add_angle_as_time(ARB2);
    EXPECT_FLOAT_EQ(do1.get_angle_as_time(), timephase1 + ARB1);
    EXPECT_FLOAT_EQ(do2.get_angle_as_time(), timephase2 + ARB2);
}

TEST_F(TestDiscreteOscillator, AssertSetAngle){

    do1.set_angle(ARB1);
    EXPECT_FLOAT_EQ(do1.get_angle(), ARB1);
}

TEST_F(TestDiscreteOscillator, AssertSetAngleTime){

    do1.set_angle_as_time(ARB1);
    EXPECT_FLOAT_EQ(do1.get_angle_as_time(), ARB1);
}

TEST_F(TestDiscreteOscillator, AssertAdvance){
    
    double timephase1 = do1.get_angle_as_time();
    double timephase2 = do2.get_angle_as_time();
    do1.advance();
    do2.advance();
    EXPECT_FLOAT_EQ(do1.get_angle_as_time(), timephase1 + TIMESTEP1);
    EXPECT_FLOAT_EQ(do2.get_angle_as_time(), timephase2 + TIMESTEP2);
}

TEST_F(TestDiscreteOscillator, AssertAmplitude){
    
    do1.advance();
    do1.advance();
    do1.advance();
    do1.advance();
    EXPECT_FLOAT_EQ(do1.get_amplitude(), sqrt(do1.get_cos() * do1.get_cos() + do1.get_sin() * do1.get_sin()));
}

TEST_F(TestDiscreteOscillator, AssertNormalize){
    
    do1.advance();
    do1.advance();
    do1.advance();
    do1.advance();
    do1.normalize();
    EXPECT_FLOAT_EQ(do1.get_amplitude(), sqrt(do1.get_cos() * do1.get_cos() + do1.get_sin() * do1.get_sin()));
}


TEST_F(TestDiscreteOscillator, AssertIQ){
    IQValue test(cos(PHASE1) * AMP, sin(PHASE1) * AMP);
    EXPECT_EQ(do1.get_cos_sin_as_IQ(), test);
}


TEST_F(TestDiscreteOscillator, AssertBackStep){

    double amp = do1.get_amplitude();
    double ang = do1.get_angle();
    do1.advance();
    do1.back_step();
    EXPECT_FLOAT_EQ(amp, do1.get_amplitude());
    EXPECT_FLOAT_EQ(ang, do1.get_angle());
}


TEST_F(TestDiscreteOscillator, AssertSerializeDeserialize){
    
    Json::Value val = do1.serialize();
    ASSERT_EQ(val["__cls"], string("DiscreteOscillator"));
    DiscreteOscillator doaux = DiscreteOscillator::deserialize(val);
    //do1.advance();
    //doaux.advance();
    ASSERT_EQ(do1, doaux);
    ASSERT_FLOAT_EQ(do1.get_angle(), doaux.get_angle());
}

TEST_F(TestDiscreteOscillator, AssertEq){

    DiscreteOscillator do3(TIMESTEP1, FREQUENCY1, PHASE1, AMP);
    do1.advance();
    do3.advance();
    do1.advance();
    do3.advance();
    ASSERT_EQ(do1, do3);

}

