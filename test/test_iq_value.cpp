/*
 * Test module for IQValue
 */

#include<gtest/gtest.h>
#include<cmath>
#include<string>
#include "iq_value.h"

#define _USE_MATH_DEFINES

#define SCALAR 3.3

#define VAL1_I 1.0
#define VAL1_Q 0.0
#define VAL2_I 0.0
#define VAL2_Q 1.0
#define VAL3_I -1.0
#define VAL3_Q 1.0
#define VAL4_I 2.3
#define VAL4_Q 5.2
#define VAL5_I 4.6
#define VAL5_Q 0.1


using namespace std;

TEST(IQValueTest, AssertAngle){

    IQValue iq_val1(VAL1_I, VAL1_Q);
    IQValue iq_val2(VAL2_I, VAL2_Q);
    IQValue iq_val3(VAL3_I, VAL3_Q);

    ASSERT_FLOAT_EQ(iq_val1.angle(), atan2(VAL1_Q, VAL1_I));
    ASSERT_FLOAT_EQ(iq_val2.angle(), atan2(VAL2_Q, VAL2_I));
    ASSERT_FLOAT_EQ(iq_val3.angle(), atan2(VAL3_Q, VAL3_I));
}

TEST(IQValueTest, AssertNorm){

    IQValue iq_val1(VAL1_I, VAL1_Q);
    IQValue iq_val2(VAL2_I, VAL2_Q);
    IQValue iq_val3(VAL3_I, VAL3_Q);

    ASSERT_FLOAT_EQ(iq_val1.norm(), sqrt(VAL1_I * VAL1_I + VAL1_Q * VAL1_Q));
    ASSERT_FLOAT_EQ(iq_val2.norm(), sqrt(VAL2_I * VAL2_I + VAL2_Q * VAL2_Q));
    ASSERT_FLOAT_EQ(iq_val3.norm(), sqrt(VAL3_I * VAL3_I + VAL3_Q * VAL3_Q));
}

TEST(IQValueTest, AssertEq){

    IQValue iq_val1(VAL4_I, VAL4_Q);
    IQValue iq_val2(VAL4_I, VAL4_Q);
    ASSERT_EQ(iq_val1, iq_val2);
}

TEST(IQValueTest, AssertI){
    IQValue iq_val1(VAL4_I, VAL4_Q);
    ASSERT_FLOAT_EQ(iq_val1.I, VAL4_I);
}  

TEST(IQValueTest, AssertQ){
    IQValue iq_val1(VAL4_I, VAL4_Q);
    ASSERT_FLOAT_EQ(iq_val1.Q, VAL4_Q);
} 

TEST(IQValueTest, AssertOperatorPlus){
    IQValue iq_val1(VAL4_I, VAL4_Q);
    IQValue iq_val2(VAL5_I, VAL5_Q);
    ASSERT_EQ(iq_val1 + iq_val2, IQValue(VAL4_I + VAL5_I, VAL4_Q + VAL5_Q));
}

TEST(IQValueTest, AssertOperatorMinus){
    IQValue iq_val1(VAL4_I, VAL4_Q);
    IQValue iq_val2(VAL5_I, VAL5_Q);
    ASSERT_EQ(iq_val1 - iq_val2, IQValue(VAL4_I - VAL5_I, VAL4_Q - VAL5_Q));
}

TEST(IQValueTest, AssertOperatorProduct){
    IQValue iq_val1(VAL4_I, VAL4_Q);
    IQValue iq_val2(VAL5_I, VAL5_Q);
    ASSERT_EQ(iq_val1 * iq_val2, IQValue(VAL4_I * VAL5_I, VAL4_Q * VAL5_Q));
}

TEST(IQValueTest, AssertOperatorProductScalar){
    IQValue iq_val1(VAL4_I, VAL4_Q);
    ASSERT_EQ(iq_val1 * SCALAR, IQValue(VAL4_I * SCALAR, VAL4_Q * SCALAR));
}

TEST(IQValueTest, AssertOperatorPlusAssign){
    IQValue iq_val1(VAL4_I, VAL4_Q);
    IQValue iq_val2(VAL5_I, VAL5_Q); 
    iq_val1 += iq_val2;
    ASSERT_EQ(iq_val1, IQValue(VAL4_I + VAL5_I, VAL4_Q + VAL5_Q));
}

TEST(IQValueTest, AssertOperatorMinusAssign){
    IQValue iq_val1(VAL4_I, VAL4_Q);
    IQValue iq_val2(VAL5_I, VAL5_Q);
    iq_val1 -= iq_val2;
    ASSERT_EQ(iq_val1, IQValue(VAL4_I - VAL5_I, VAL4_Q - VAL5_Q));
}

TEST(IQValueTest, AssertScalar){
    IQValue iq_val1(VAL4_I, VAL4_Q);
    IQValue iq_val2(VAL5_I, VAL5_Q);
    ASSERT_EQ(iq_val1.scalar(iq_val2), VAL4_I * VAL5_I + VAL4_Q * VAL5_Q);
}

TEST(IQValueTest, AssertSerialize){
    Json::Value val;
    val["__cls"] = string("IQValue");
    val["I"] = VAL4_I;
    val["Q"] = VAL4_Q;
    IQValue iq_val(VAL4_I, VAL4_Q);
    ASSERT_EQ(val, iq_val.serialize());
}

TEST(IQValueTest, AssertDeserialize){
    Json::Value val;
    val["__cls"] = string("IQValue");
    val["I"] = VAL4_I;
    val["Q"] = VAL4_Q;
    IQValue iq_val(VAL4_I, VAL4_Q);
    IQValue iq_val2 = IQValue::deserialize(val);
    ASSERT_EQ(iq_val, iq_val2);
}

