#include<gtest/gtest.h>
#include<cmath>
#include "cavity.h"

#define TIMESTEP 0.005
#define INT_STEP 10000
#define OSC1_FREQ 3.0
#define OSC2_FREQ 4.0
#define QL 10000.0
#define RoQ 1.0
#define KFRAC 1/QL
#define K KFRAC*(1.0/(QL*QL*RoQ*RoQ))
#define EPSILON 0.01
#define MICRO_FREQ 0.03
#define MICRO_AMP 0.003
#define BUNCH_CHARGE 1.0
#define BUNCH_SEPARATION 8
#define BEAM_START 100.0
#define N_BUNCHES 10

#define _USE_MATH_DEFINES
using namespace std;

TEST(CavityTest, AssertEq){

    Cavity cav1(INT_STEP, TIMESTEP, OSC1_FREQ, QL, RoQ);
    Cavity cav2(INT_STEP, TIMESTEP, OSC1_FREQ, QL, RoQ);
    DiscreteOscillator osc1(TIMESTEP, OSC1_FREQ);
    DiscreteOscillator osc2(TIMESTEP, OSC2_FREQ);
    cav1.add_power_source(osc1);
    cav2.add_power_source(osc1);
    cav1.add_power_source(osc2);
    cav2.add_power_source(osc2);
    LorentzForceDetuning lfd(TIMESTEP, K);
    cav1.set_lfd(lfd);
    cav2.set_lfd(lfd);
    DiscreteOscillator micro_source(TIMESTEP, MICRO_FREQ, 0.0, MICRO_AMP);
    cav1.add_microphonic_source(micro_source);
    cav2.add_microphonic_source(micro_source);
    BeamLoading bl(TIMESTEP, OSC1_FREQ, BUNCH_CHARGE, BUNCH_SEPARATION, 0.0, BEAM_START, N_BUNCHES);
    cav1.set_beam_loading(bl);
    cav2.set_beam_loading(bl);

    ASSERT_EQ(cav1, cav2);

    Cavity cav3(INT_STEP, TIMESTEP, OSC1_FREQ, QL, RoQ);
    Cavity cav4(INT_STEP, TIMESTEP, OSC1_FREQ, QL, RoQ);
    
    ASSERT_EQ(cav3, cav4);
};

TEST(CavityTest, AssertGradientWithLFDCompensated){

    Cavity cav(INT_STEP, TIMESTEP, OSC1_FREQ - KFRAC, QL, RoQ);
    DiscreteOscillator osc(TIMESTEP, OSC1_FREQ);
    LorentzForceDetuning lfd(TIMESTEP, K);
    cav.add_power_source(osc);
    cav.set_lfd(lfd);
    vector<IQValue> ff;
    ff.push_back(IQValue(1.0,0));
    
    for(unsigned int i=0; i< 10.0*(QL/(INT_STEP * TIMESTEP * OSC1_FREQ)); i++){

        cav.macro_step(ff);
    } 

    IQValue result = cav.get_V();
    ASSERT_LT(result.I, QL*RoQ*(1.0 + EPSILON));
    ASSERT_GT(result.I, QL*RoQ*(1.0 - EPSILON));
    ASSERT_LT(abs(result.angle()), (2.0 * OSC1_FREQ * M_PI * TIMESTEP));
};

TEST(CavityTest, AssertSerializeDeserialize){

    Cavity cav1(INT_STEP, TIMESTEP, OSC1_FREQ, QL, RoQ);
    DiscreteOscillator osc1(TIMESTEP, OSC1_FREQ);
    DiscreteOscillator osc2(TIMESTEP, OSC2_FREQ);
    cav1.add_power_source(osc1);
    cav1.add_power_source(osc2);
    LorentzForceDetuning lfd(TIMESTEP, K);
    cav1.set_lfd(lfd);
    DiscreteOscillator micro_source(TIMESTEP, MICRO_FREQ, 0.0, MICRO_AMP);
    cav1.add_microphonic_source(micro_source);
    BeamLoading bl(TIMESTEP, OSC1_FREQ, BUNCH_CHARGE, BUNCH_SEPARATION, 0.0, BEAM_START, N_BUNCHES);
    cav1.set_beam_loading(bl);
    Json::Value serialized_cavity = cav1.serialize();
    Cavity cav2 = Cavity::deserialize(serialized_cavity);

    ASSERT_EQ(cav1, cav2);
};


