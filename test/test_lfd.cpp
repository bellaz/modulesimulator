/*
 * Test module for LorentzForceDetuning
 */

#include<gtest/gtest.h>
#include<cmath>
#include<string>
#include "lfd.h"

#define _USE_MATH_DEFINES

#define TIMESTEP 0.32
#define K_LFD 0.2
#define V1 0.3
#define V2 0.9
#define V3 1.7
#define V4 1.9
#define A1 0.3
#define B1 3.3
#define C1 1.1
#define CONST_VOLT(x) (C1)
#define LINE_VOLT(x) (B1*x+C1)
#define CURVE_VOLT(x) (A1*x*x+B1*x+C1)

using namespace std;

TEST(TestLorentzForceDetuning, AssertEq){
    LorentzForceDetuning lfd1(TIMESTEP, K_LFD);
    LorentzForceDetuning lfd2(TIMESTEP, K_LFD);
    lfd1.step(V1);
    lfd2.step(V1);
    lfd1.step(V2);
    lfd2.step(V2);
    lfd1.step(V3);
    lfd2.step(V3);
    lfd1.step(V4);
    lfd2.step(V4);

    ASSERT_EQ(lfd1.get_frequency_delta(), lfd2.get_frequency_delta());
    ASSERT_EQ(lfd1, lfd2);

}

TEST(TestLorentzForceDetuning, AssertSerializeDeserialize){
    
    LorentzForceDetuning lfd1(TIMESTEP, K_LFD);
    lfd1.step(V1);
    lfd1.step(V2);
    lfd1.step(V3);
    lfd1.step(V4);
    Json::Value serialized_lfd = lfd1.serialize();
    LorentzForceDetuning lfd2 = LorentzForceDetuning::deserialize(serialized_lfd);
    ASSERT_EQ(lfd1, lfd2);
}

TEST(TestLorentzForceDetuning, AssertFrequencyConstant){
    
    LorentzForceDetuning lfd1(TIMESTEP, K_LFD);
    lfd1.step(CONST_VOLT(0));
    lfd1.step(CONST_VOLT(1));
    lfd1.step(CONST_VOLT(2));
    ASSERT_FLOAT_EQ(lfd1.get_frequency_delta(), CONST_VOLT(2.5) * CONST_VOLT(2.5) *  K_LFD);
}

TEST(TestLorentzForceDetuning, AssertFrequencyLinear){
    
    LorentzForceDetuning lfd1(TIMESTEP, K_LFD);
    lfd1.step(LINE_VOLT(0));
    lfd1.step(LINE_VOLT(1));
    lfd1.step(LINE_VOLT(2));
    ASSERT_FLOAT_EQ(lfd1.get_frequency_delta(), LINE_VOLT(2.5) * LINE_VOLT(2.5) *  K_LFD);
}

TEST(TestLorentzForceDetuning, AssertFrequencyCurve){
    
    LorentzForceDetuning lfd1(TIMESTEP, K_LFD);
    lfd1.step(CURVE_VOLT(0));
    lfd1.step(CURVE_VOLT(1));
    lfd1.step(CURVE_VOLT(2));
    ASSERT_FLOAT_EQ(lfd1.get_frequency_delta(), CURVE_VOLT(2.5) * CURVE_VOLT(2.5) *  K_LFD);
}

