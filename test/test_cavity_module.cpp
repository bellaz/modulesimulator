#include<gtest/gtest.h>
#include<cmath>
#include "cavity_module.h"
#include "cavity_module_factory.h"

#define _USE_MATH_DEFINES

#define TIMESTEP 0.008
#define INT_STEP 10000
#define OSC1_FREQ 3.0
#define OSC2_FREQ 4.0
#define QL 1000000.0
#define RoQ 1.0
#define EPSILON 0.01
#define N_QS 10

#define DELTA_F1 0.0
#define DELTA_F2 (+OSC1_FREQ*0.5/QL)
#define DELTA_F3 (-OSC1_FREQ*0.5/QL)
#define DELTA_F4 0.0

#define CAV_FREQ1 (OSC1_FREQ+DELTA_F1)
#define CAV_FREQ2 (OSC1_FREQ+DELTA_F2) 
#define CAV_FREQ3 (OSC1_FREQ+DELTA_F3)
#define CAV_FREQ4 (OSC2_FREQ+DELTA_F4)

#define ANGLE1 atan(-2.0*QL*DELTA_F1/OSC1_FREQ)
#define ANGLE2 atan(-2.0*QL*DELTA_F2/OSC1_FREQ)
#define ANGLE3 atan(-2.0*QL*DELTA_F3/OSC1_FREQ)
#define ANGLE4 atan(-2.0*QL*DELTA_F4/OSC2_FREQ)

#define AMP1 (QL*RoQ/sqrt(1.0+pow(-2.0*QL*DELTA_F1/OSC1_FREQ,2.0))) 
#define AMP2 (QL*RoQ/sqrt(1.0+pow(-2.0*QL*DELTA_F2/OSC1_FREQ,2.0))) 
#define AMP3 (QL*RoQ/sqrt(1.0+pow(-2.0*QL*DELTA_F3/OSC1_FREQ,2.0))) 
#define AMP4 (QL*RoQ/sqrt(1.0+pow(-2.0*QL*DELTA_F4/OSC2_FREQ,2.0))) 

using namespace std;

class CavityModuleTest : public ::testing::Test {

    public:

        virtual void SetUp() {

            fac = CavityModuleFactory(TIMESTEP, INT_STEP);
            fac.add_cavity(CAV_FREQ1, QL,RoQ);
            fac.add_cavity(CAV_FREQ2, QL,RoQ);
            fac.add_cavity(CAV_FREQ3, QL,RoQ);
            fac.add_cavity(CAV_FREQ4, QL,RoQ);

            fac.add_power_source(OSC1_FREQ);
            fac.add_power_source(OSC2_FREQ);
            fac.set_power_source(3,1);
            mod1 = new CavityModule(fac.to_cavity_module());
            mod2 = new CavityModule(fac.to_cavity_module());
        };

        virtual void TearDown() {

            delete mod1;
            delete mod2;
        }

        CavityModuleFactory fac;
        CavityModule* mod1,* mod2;
};

TEST_F(CavityModuleTest, AssertEq) {

    ASSERT_EQ(*mod1, *mod2);
}

TEST_F(CavityModuleTest, AssertSerializeDeserialize){

    Json::Value serialized_module = mod1->serialize();
    CavityModule deserialized_module = CavityModule::deserialize(serialized_module);
    ASSERT_EQ(*mod1, deserialized_module);
}

TEST_F(CavityModuleTest, AssertGradient) {

    vector<IQValue> iqvalue = {IQValue(1.0, 0.0), IQValue(1.0, 0.0)};

    for(int i = 0; i < (int)(QL * N_QS/(INT_STEP * TIMESTEP * OSC1_FREQ)); i++){
            
                mod1->macro_step(iqvalue);
            }

    vector<IQValue> results = mod1->macro_step(iqvalue);
    ASSERT_EQ(results.size(), 4);
    ASSERT_GT(results[0].norm(), AMP1*(1.0-EPSILON));
    ASSERT_GT(results[1].norm(), AMP2*(1.0-EPSILON));
    ASSERT_GT(results[2].norm(), AMP3*(1.0-EPSILON));
    ASSERT_GT(results[3].norm(), AMP4*(1.0-EPSILON));
    ASSERT_LT(results[0].norm(), AMP1*(1.0+EPSILON));
    ASSERT_LT(results[1].norm(), AMP2*(1.0+EPSILON));
    ASSERT_LT(results[2].norm(), AMP3*(1.0+EPSILON));
    ASSERT_LT(results[3].norm(), AMP4*(1.0+EPSILON));
    ASSERT_GT(results[1].angle(), ANGLE2-abs(ANGLE2)*EPSILON);    
    ASSERT_GT(results[2].angle(), ANGLE3-abs(ANGLE3)*EPSILON);    
    ASSERT_LT(abs(results[0].angle()), (2.0 * M_PI * TIMESTEP * OSC1_FREQ));    
    ASSERT_LT(results[1].angle(), ANGLE2+abs(ANGLE2)*EPSILON);    
    ASSERT_LT(results[2].angle(), ANGLE3+abs(ANGLE3)*EPSILON);    
    ASSERT_LT(abs(results[3].angle()), (2.0 * M_PI * TIMESTEP * OSC2_FREQ));    
}

TEST_F(CavityModuleTest, AssertCavityIQArgumentsException) {

    vector<IQValue> iqvalue;
    iqvalue.push_back(IQValue(1.0, 0.0));
    ASSERT_THROW(mod1->macro_step(iqvalue), CavityIQArgumentsException);
    iqvalue.push_back(IQValue(1.0, 0.0));
    iqvalue.push_back(IQValue(1.0, 0.0));
    ASSERT_THROW(mod2->macro_step(iqvalue), CavityIQArgumentsException);
};
