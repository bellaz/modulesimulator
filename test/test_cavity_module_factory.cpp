#include<gtest/gtest.h>
#include<cmath>
#include "cavity_module.h"
#include "cavity_module_factory.h"

#define TIMESTEP 0.005
#define INT_STEP 10000
#define OSC1_FREQ 3.0
#define OSC2_FREQ 4.0
#define QL 10000.0
#define RoQ 1.0
#define KFRAC 0.5 / QL
#define K KFRAC*(1.0/(QL*QL*RoQ*RoQ))
#define EPSILON 0.01
#define MICRO_FREQ 0.03
#define MICRO_AMP 0.003
#define BUNCH_CHARGE 1.0
#define BUNCH_SEPARATION 8
#define BEAM_START 100.0
#define N_BUNCHES 10

#define _USE_MATH_DEFINES
using namespace std;

class CavityModuleFactoryTest : public ::testing::Test {

    public:

    virtual void SetUp() {

    fac1 = CavityModuleFactory(INT_STEP, TIMESTEP);
    fac1.add_cavity(OSC1_FREQ,QL,RoQ);
    fac1.add_cavity(OSC2_FREQ,QL,RoQ);
    fac1.add_power_source(OSC1_FREQ);
    fac1.add_power_source(OSC2_FREQ);
    fac1.add_microphonic_source(MICRO_FREQ, 0.0, MICRO_AMP);
    fac1.set_beam_loading(OSC1_FREQ, BUNCH_CHARGE, BUNCH_SEPARATION, 0.0, BEAM_START, N_BUNCHES);
    fac1.set_lfd(K);

    fac2 = CavityModuleFactory(INT_STEP, TIMESTEP);
    fac2.add_cavity(OSC1_FREQ,QL,RoQ);
    fac2.add_cavity(OSC2_FREQ,QL,RoQ);
    fac2.add_power_source(OSC1_FREQ);
    fac2.add_power_source(OSC2_FREQ);
    fac2.add_microphonic_source(MICRO_FREQ, 0.0, MICRO_AMP);
    fac2.set_beam_loading(OSC1_FREQ, BUNCH_CHARGE, BUNCH_SEPARATION, 0.0, BEAM_START, N_BUNCHES);
    fac2.set_lfd(K);

};

    CavityModuleFactory fac1, fac2;
};

TEST_F(CavityModuleFactoryTest, AssertEq) {

    ASSERT_EQ(fac1, fac2);
};

TEST_F(CavityModuleFactoryTest, AssertSerializeDeserialize) {

    Json::Value serialized_factory = fac1.serialize();
    CavityModuleFactory deserialized_factory = CavityModuleFactory::deserialize(serialized_factory);
    ASSERT_EQ(fac1, deserialized_factory);
    
};

TEST_F(CavityModuleFactoryTest, AssertReconversion) {
    
    CavityModule cav1 = fac1.to_cavity_module();
    fac2 = cav1.to_cavity_module_factory();
    ASSERT_EQ(fac1, fac2);
};

TEST_F(CavityModuleFactoryTest, AssertCavityIndexBoundaryException) {

    ASSERT_THROW(fac1.set_power_source(2, 0), CavityIndexBoundaryException);
};

TEST_F(CavityModuleFactoryTest, AssertSourceIndexBoundaryException) {

    ASSERT_NO_THROW(fac1.set_power_source(1, 10));
    ASSERT_THROW(fac1.to_cavity_module(), SourceIndexBoundaryException);
}

TEST_F(CavityModuleFactoryTest, AssertNoCavitiesException){

    fac1.delete_cavities();
    ASSERT_THROW(fac1.to_cavity_module(), NoCavitiesException);
}
