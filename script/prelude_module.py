#!/usr/bin/env python3
'''
    This script simulate a eight cavity module with random parameters distribution
'''
import math
import random
import matplotlib.pyplot as plt
import pyximport
pyximport.install()
import cavity_module as mod


SIM_TIME = 1.3E-3
STEP_TIME = 1.0/1.0E11
INT_STEPS = 10000
GEN_FREQ = 1.3E9
GEN_AMP = 1.0

CAV_FREQ = 1.3E9
CAV_Q = 3.0E6
CAV_ROQ = 2080.0

FF_TIME_RATIO = 0.3
FF_AMP_RATIO = 1.0 - math.exp(-FF_TIME_RATIO *
                              SIM_TIME *
                              math.pi *
                              CAV_FREQ /
                              CAV_Q)


STEPS = int(SIM_TIME/(STEP_TIME * INT_STEPS))
FF_STEP = int(STEPS * FF_TIME_RATIO)

DET_FRAC = 1/(2*CAV_Q)
DET_COEFF = 1/((2*CAV_Q)*1.0E20)
DET_Q = 0.4

CAV_FREQ *= (1 - DET_COEFF * 0.5E20)

random.seed()
cavities = []
cavities.append(mod.Cavity(CAV_FREQ*(1 + random.uniform(-DET_FRAC, DET_FRAC)), CAV_Q*(1+ random.uniform(-DET_Q, DET_Q)), CAV_ROQ, random.uniform(0.0, DET_COEFF)))
cavities.append(mod.Cavity(CAV_FREQ*(1 + random.uniform(-DET_FRAC, DET_FRAC)), CAV_Q*(1 + random.uniform(-DET_Q, DET_Q)), CAV_ROQ, random.uniform(0.0, DET_COEFF)))
cavities.append(mod.Cavity(CAV_FREQ*(1 + random.uniform(-DET_FRAC, DET_FRAC)), CAV_Q*(1 + random.uniform(-DET_Q, DET_Q)), CAV_ROQ, random.uniform(0.0, DET_COEFF)))
cavities.append(mod.Cavity(CAV_FREQ*(1 + random.uniform(-DET_FRAC, DET_FRAC)), CAV_Q*(1 + random.uniform(-DET_Q, DET_Q)), CAV_ROQ, random.uniform(0.0, DET_COEFF)))
cavities.append(mod.Cavity(CAV_FREQ*(1 + random.uniform(-DET_FRAC, DET_FRAC)), CAV_Q*(1 + random.uniform(-DET_Q, DET_Q)), CAV_ROQ, random.uniform(0.0, DET_COEFF)))
cavities.append(mod.Cavity(CAV_FREQ*(1 + random.uniform(-DET_FRAC, DET_FRAC)), CAV_Q*(1 + random.uniform(-DET_Q, DET_Q)), CAV_ROQ, random.uniform(0.0, DET_COEFF)))
cavities.append(mod.Cavity(CAV_FREQ*(1 + random.uniform(-DET_FRAC, DET_FRAC)), CAV_Q*(1 + random.uniform(-DET_Q, DET_Q)), CAV_ROQ, random.uniform(0.0, DET_COEFF)))
cavities.append(mod.Cavity(CAV_FREQ*(1 + random.uniform(-DET_FRAC, DET_FRAC)), CAV_Q*(1 + random.uniform(-DET_Q, DET_Q)), CAV_ROQ, random.uniform(0.0, DET_COEFF)))

n_cav = len(cavities)
results_amp = [[] for _ in range(n_cav + 2)]
results_pha = [[] for _ in range(n_cav + 2)]

msim = mod.Module_Simulator(STEP_TIME, INT_STEPS, GEN_FREQ, cavities)
macro_step_time = INT_STEPS * STEP_TIME
time = 0.0

for j in range(STEPS):
    if j == FF_STEP:
        GEN_AMP *= FF_AMP_RATIO

    res = msim.step(GEN_AMP, 0.0)
    Isum = 0.0
    Qsum = 0.0
    for i in range(n_cav):
        I = res[i][1]
        Q = res[i][2]
        results_amp[i].append(math.sqrt(I*I + Q*Q))
        results_pha[i].append(180.0 * math.atan2(Q, -I)/math.pi)
        Isum += I
        Qsum += Q

    results_amp[n_cav].append(math.sqrt(Isum*Isum + Qsum*Qsum)/n_cav)
    results_pha[n_cav].append(180.0 * math.atan2(Qsum, -Isum)/math.pi)
    results_amp[n_cav+1].append(time)
    results_pha[n_cav+1].append(time)
    time += macro_step_time

plt.figure(1)
plt.title('Cavity Module - Amplitude Plot')
plt.xlabel('time (s)')
plt.ylabel('amplitude')
for i in range(n_cav):
    plt.plot(results_amp[n_cav+1], results_amp[i])

plt.plot(results_amp[n_cav+1], results_amp[n_cav], 'k--')

plt.draw()

fig2 = plt.figure(2)
plt.title('Cavity Module - Phase Plot')
plt.xlabel('time (s)')
plt.ylabel('phase (deg)')
for i in range(n_cav):
    plt.plot(results_pha[n_cav+1], results_pha[i])

plt.plot(results_pha[n_cav+1], results_pha[n_cav], 'k--')
plt.show()

