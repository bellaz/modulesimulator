#!/usr/bin/env python3
'''
    This module should simulate an XFEL cavity
'''
import math
import random
import matplotlib.pyplot as plt
import pyximport
import numpy as np
pyximport.install()
import module_simulator as mod
from loop_delay import LoopDelayer

SIM_TIME = 1.3E-3
STEP_TIME = 1.0/1.3E12
SAMPLING_FREQUENCY = 9.0E6
INT_STEPS = 110000#iint(1.0/(SAMPLING_FREQUENCY * STEP_TIME))
GEN_FREQ = 1.3E9

TARGET_FF_AMP = 20E6

CAV_TARGET_FREQ = 1.3E9
CAV_TARGET_Q = 3.0E6
CAV_TARGET_ROQ = 1.0

START_FB_TIME = 0.2E-3
START_FLATTOP_TIME = 0.8E-3

random.seed()

UNCORRELATED_NOISE_AMPLITUDE = 1.0E-7 * TARGET_FF_AMP

CAV_FREQ = CAV_TARGET_FREQ + 0.1 * CAV_TARGET_FREQ / CAV_TARGET_Q
CAV_Q = CAV_TARGET_Q * 1.2
CAV_ROQ = CAV_TARGET_ROQ

NORMALIZED_GAIN = 100.0
GAIN = NORMALIZED_GAIN / CAV_TARGET_Q

FF_RATIO = (1.0 - math.exp(-START_FLATTOP_TIME * math.pi * CAV_TARGET_FREQ / CAV_TARGET_Q))
FLATTOP_FF_AMP = TARGET_FF_AMP / (CAV_TARGET_Q * CAV_TARGET_ROQ)
FILLING_FF_AMP = FLATTOP_FF_AMP / FF_RATIO 
FLATTOP_SP_AMP = TARGET_FF_AMP
FILLING_SP_AMP = FLATTOP_SP_AMP / FF_RATIO


def get_ff(time):
    if time < START_FLATTOP_TIME:
        return np.array([FILLING_FF_AMP, 0.0])
    else:
        return np.array([FLATTOP_FF_AMP, 0.0])

def get_sp(time):
    if time < START_FLATTOP_TIME:
        return np.array([FILLING_SP_AMP, 0.0]) * (1.0 - math.exp(-time * math.pi * CAV_TARGET_FREQ / CAV_TARGET_Q))
    else:
        return np.array([FLATTOP_SP_AMP, 0.0])

STEPS = int(SIM_TIME/(STEP_TIME * INT_STEPS))

#LFD_PHASE_SHIFT_AT_MAXIMUM_GRADIENT = 1
#LFD_K = LFD_PHASE_SHIFT_AT_MAXIMUM_GRADIENT/(CAV_ROQ * CAV_ROQ * CAV_Q * CAV_Q * CAV_Q * GEN_AMP * GEN_AMP)

f = mod.Factory(STEP_TIME, INT_STEPS)
f.add_cavity(CAV_FREQ, CAV_Q, CAV_ROQ)
#f.set_lfd(LFD_K)

n_cav = 1
results_amp = [[] for _ in range(n_cav + 2)]
results_pha = [[] for _ in range(n_cav + 2)]

f.add_power_source(GEN_FREQ)

m = f.to_module()

macro_step_time = INT_STEPS * STEP_TIME
time = 0.0

res = [(0.0, 0.0)]

delayer = LoopDelayer(3.0/SAMPLING_FREQUENCY, 1.0/SAMPLING_FREQUENCY)

for j in range(STEPS):

    nc_noise = np.array([random.uniform(UNCORRELATED_NOISE_AMPLITUDE, -UNCORRELATED_NOISE_AMPLITUDE),
                        random.uniform(UNCORRELATED_NOISE_AMPLITUDE, -UNCORRELATED_NOISE_AMPLITUDE)])
    error_abs = -(np.array(res[0]) - get_sp(time) + nc_noise) * GAIN

    error_abs_delayed = delayer.push_pop(error_abs)
    res = m.macro_step([get_ff(time) + error_abs_delayed])

    Isum = 0.0
    Qsum = 0.0
    for i in range(n_cav):
        
        I = res[i][0]
        Q = res[i][1]
        results_amp[i].append(math.sqrt(I*I + Q*Q))
        results_pha[i].append(180.0 * math.atan2(Q, I)/math.pi)
        Isum += I
        Qsum += Q

    results_amp[n_cav].append(math.sqrt(Isum*Isum + Qsum*Qsum)/n_cav)
    results_pha[n_cav].append(180.0 * math.atan2(Qsum, Isum)/math.pi)
    results_amp[n_cav+1].append(time)
    results_pha[n_cav+1].append(time)
    time += macro_step_time

plt.figure(1)
plt.title('Cavity Module - Amplitude Plot')
plt.xlabel('time (s)')
plt.ylabel('amplitude')
for i in range(n_cav):
    plt.plot(results_amp[n_cav+1], results_amp[i])

plt.plot(results_amp[n_cav+1], results_amp[n_cav], 'k--')

plt.draw()

fig2 = plt.figure(2)
plt.title('Cavity Module - Phase Plot')
plt.xlabel('time (s)')
plt.ylabel('phase (deg)')
for i in range(n_cav):
    plt.plot(results_pha[n_cav+1], results_pha[i])

plt.plot(results_pha[n_cav+1], results_pha[n_cav], 'k--')
plt.show()

