#!/usr/bin/env python3
'''
    This module should simulate an XFEL cavity
'''
import math
import random
import matplotlib.pyplot as plt
import pyximport
import numpy as np
import module_simulator as mod
import module_simulator.envelope as env
import module_simulator.envelope.core as envc

SIM_TIME = 1.3E-2
STEP_TIME = 1.0/1.3E12
INT_STEPS = 100000
GEN_FREQ = 1.3E9

TARGET_FF_AMP = 20E6

CAV_TARGET_FREQ = 1.3E9
CAV_TARGET_Q = 2.0E7
CAV_TARGET_ROQ = 1.0

START_FB_TIME = 0.2E-3
START_FLATTOP_TIME = 0.8E-3

K_LFD = -0.1E-12

random.seed()

UNCORRELATED_NOISE_AMPLITUDE = 800.0

MICRO_FREQ1 = 49
MICRO_AMPL1 = 1
MICRO_FREQ2 = 57
MICRO_AMPL2 = 2

MECH_FREQ1 = 42.72
MECH_Q1    = 30.5
MECH_COUP1 = 0.0105E-12

MECH_FREQ2 = 101.3
MECH_Q2    = 848.4
MECH_COUP2 = 0.0108E-12

MECH_FREQ3 = 172.09
MECH_Q3    = 705.3
MECH_COUP3 = 0.0126E-12

MECH_FREQ4 = 232.0
MECH_Q4    = 23.2
MECH_COUP4 = 0.0098E-12 

MECH_FREQ5 = 281.8
MECH_Q5    = 108.4
MECH_COUP5 = 0.0084E-12

CAV_FREQ = CAV_TARGET_FREQ - (K_LFD + MECH_COUP1 + MECH_COUP2 + MECH_COUP3 + MECH_COUP4 + MECH_COUP5) * TARGET_FF_AMP * TARGET_FF_AMP
CAV_Q = CAV_TARGET_Q * 1.00
CAV_ROQ = CAV_TARGET_ROQ

NORMALIZED_GAIN = 0.0
GAIN = NORMALIZED_GAIN / CAV_TARGET_Q

FF_RATIO = (1.0 - math.exp(-START_FLATTOP_TIME * math.pi * CAV_TARGET_FREQ / CAV_TARGET_Q))
FLATTOP_FF_AMP = TARGET_FF_AMP / (CAV_TARGET_Q * CAV_TARGET_ROQ)
FILLING_FF_AMP = FLATTOP_FF_AMP / FF_RATIO 
FLATTOP_SP_AMP = TARGET_FF_AMP
FILLING_SP_AMP = FLATTOP_SP_AMP / FF_RATIO


def get_ff(time):
    if time < START_FLATTOP_TIME:
        return np.array([FILLING_FF_AMP, 0.0])
    else:
        return np.array([FLATTOP_FF_AMP, 0.0])

def get_sp(time):
    if time < START_FLATTOP_TIME:
        return np.array([FILLING_SP_AMP, 0.0]) * (1.0 - math.exp(-time * math.pi * CAV_TARGET_FREQ / CAV_TARGET_Q))
    else:
        return np.array([FLATTOP_SP_AMP, 0.0])

STEPS = int(SIM_TIME/(STEP_TIME * INT_STEPS))

#LFD_PHASE_SHIFT_AT_MAXIMUM_GRADIENT = 1
#LFD_K = LFD_PHASE_SHIFT_AT_MAXIMUM_GRADIENT/(CAV_ROQ * CAV_ROQ * CAV_Q * CAV_Q * CAV_Q * GEN_AMP * GEN_AMP)

f = mod.Factory(STEP_TIME, INT_STEPS)
f.add_power_source(GEN_FREQ)
f.add_cavity(CAV_FREQ, CAV_Q, CAV_ROQ)
f.set_lfd(K_LFD)
#f.add_microphonic_source(MICRO_FREQ1, 0, MICRO_AMPL1)
#f.add_microphonic_source(MICRO_FREQ2, 0, MICRO_AMPL2)
f.add_mechanical_resonance(MECH_FREQ1, MECH_Q1, MECH_COUP1)
f.add_mechanical_resonance(MECH_FREQ2, MECH_Q2, MECH_COUP2)
f.add_mechanical_resonance(MECH_FREQ3, MECH_Q3, MECH_COUP3)
f.add_mechanical_resonance(MECH_FREQ4, MECH_Q4, MECH_COUP4)
f.add_mechanical_resonance(MECH_FREQ5, MECH_Q5, MECH_COUP5)
m = f.to_module()

fenv = env.Factory(STEP_TIME * INT_STEPS, GEN_FREQ)
fenv.add_cavity(CAV_FREQ, CAV_Q, CAV_ROQ, (TARGET_FF_AMP, 0.0))
fenv.set_lfd(K_LFD)
fenv.add_microphonic_source(MICRO_FREQ1, 0, MICRO_AMPL1)
fenv.add_microphonic_source(MICRO_FREQ2, 0, MICRO_AMPL2)
fenv.add_mechanical_resonance(MECH_FREQ1, MECH_Q1, MECH_COUP1)
fenv.add_mechanical_resonance(MECH_FREQ2, MECH_Q2, MECH_COUP2)
fenv.add_mechanical_resonance(MECH_FREQ3, MECH_Q3, MECH_COUP3)
fenv.add_mechanical_resonance(MECH_FREQ4, MECH_Q4, MECH_COUP4)
fenv.add_mechanical_resonance(MECH_FREQ5, MECH_Q5, MECH_COUP5)
menv = fenv.to_module()

fenvc = envc.Factory(STEP_TIME * INT_STEPS, GEN_FREQ)
fenvc.add_cavity(CAV_FREQ, CAV_Q, CAV_ROQ, (TARGET_FF_AMP, 0.0))
fenvc.set_lfd(K_LFD)
fenvc.add_microphonic_source(MICRO_FREQ1, 0, MICRO_AMPL1)
fenvc.add_microphonic_source(MICRO_FREQ2, 0, MICRO_AMPL2)
fenvc.add_mechanical_resonance(MECH_FREQ1, MECH_Q1, MECH_COUP1)
fenvc.add_mechanical_resonance(MECH_FREQ2, MECH_Q2, MECH_COUP2)
fenvc.add_mechanical_resonance(MECH_FREQ3, MECH_Q3, MECH_COUP3)
fenvc.add_mechanical_resonance(MECH_FREQ4, MECH_Q4, MECH_COUP4)
fenvc.add_mechanical_resonance(MECH_FREQ5, MECH_Q5, MECH_COUP5)
fenvc.set_beam_loading(-1.0E-9, 24, math.pi, SIM_TIME/2.0, int(0.25 * SIM_TIME /(INT_STEPS * STEP_TIME * 24)))
menvc = fenvc.to_module()


n_cav = 4
results_amp = [[] for _ in range(n_cav + 1)]
results_pha = [[] for _ in range(n_cav + 1)]



macro_step_time = INT_STEPS * STEP_TIME
time = 0.0

res = [(0.0, 0.0)]
resenv = [(0.0, 0.0)]

for j in range(STEPS):

    #ff_now = get_ff(time)
    ff_now = get_ff(SIM_TIME)
    #res = m.macro_step([ff_now])
    #resenv = menv.macro_step(ff_now[0], ff_now[1])
    resenvc = menvc.macro_step(ff_now[0], ff_now[1])
    

    I = res[0][0]
    Q = res[0][1]

    results_amp[0].append(math.sqrt(I*I + Q*Q))
    results_pha[0].append(180.0 * math.atan2(Q, I)/math.pi)

    I = resenv[0][0]
    Q = resenv[0][1]

    results_amp[1].append(math.sqrt(I*I + Q*Q))
    results_pha[1].append(180.0 * math.atan2(Q, I)/math.pi)

    I = resenvc[0][0]
    Q = resenvc[0][1]

    results_amp[2].append(math.sqrt(I*I + Q*Q))
    results_pha[2].append(180.0 * math.atan2(Q, I)/math.pi)


    results_amp[3].append(get_sp(time)[0])
    results_pha[3].append(0.0)

    results_amp[4].append(time)
    results_pha[4].append(time)
    time += macro_step_time

plt.figure()
plt.title('Cavity Module - Amplitude Plot')
plt.xlabel('time (s)')
plt.ylabel('amplitude')
for i in range(n_cav):
    plt.plot(results_amp[n_cav], results_amp[i])


plt.draw()

plt.figure()
plt.title('Cavity Module - Phase Plot')
plt.xlabel('time (s)')
plt.ylabel('phase (deg)')
for i in range(n_cav):
    plt.plot(results_pha[n_cav], results_pha[i])

plt.show()

