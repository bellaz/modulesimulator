#!/usr/bin/env python3
'''
    This module should simulate an XFEL cavity
'''
import math
import random
import matplotlib.pyplot as plt
import pyximport
pyximport.install()
import module_simulator as mod


SIM_TIME = 1.3E-3
STEP_TIME = 1.0/1.3E12
INT_STEPS = 100000
GEN_FREQ = 1.3E9
GEN_AMP = 1.0

CAV_FREQ = 1.3E9
CAV_Q = 1.0E5
CAV_ROQ = 1.0

STEPS = int(SIM_TIME/(STEP_TIME * INT_STEPS))


LFD_PHASE_SHIFT_AT_MAXIMUM_GRADIENT = 1
LFD_K = LFD_PHASE_SHIFT_AT_MAXIMUM_GRADIENT/(CAV_ROQ * CAV_ROQ * CAV_Q * CAV_Q * CAV_Q * GEN_AMP * GEN_AMP)

f = mod.Factory(STEP_TIME, INT_STEPS)
f.add_cavity(CAV_FREQ, CAV_Q, CAV_ROQ)
f.set_lfd(LFD_K)

n_cav = 1
results_amp = [[] for _ in range(n_cav + 2)]
results_pha = [[] for _ in range(n_cav + 2)]

f.add_power_source(GEN_FREQ)

m = f.to_module()

macro_step_time = INT_STEPS * STEP_TIME
time = 0.0

for j in range(STEPS):
    
    res = m.macro_step([(GEN_AMP, 0.0)])
    Isum = 0.0
    Qsum = 0.0
    for i in range(n_cav):
        I = res[i][0]
        Q = res[i][1]
        results_amp[i].append(math.sqrt(I*I + Q*Q))
        results_pha[i].append(180.0 * math.atan2(Q, I)/math.pi)
        Isum += I
        Qsum += Q

    results_amp[n_cav].append(math.sqrt(Isum*Isum + Qsum*Qsum)/n_cav)
    results_pha[n_cav].append(180.0 * math.atan2(Qsum, Isum)/math.pi)
    results_amp[n_cav+1].append(time)
    results_pha[n_cav+1].append(time)
    time += macro_step_time

plt.figure(1)
plt.title('Cavity Module - Amplitude Plot')
plt.xlabel('time (s)')
plt.ylabel('amplitude')
for i in range(n_cav):
    plt.plot(results_amp[n_cav+1], results_amp[i])

plt.plot(results_amp[n_cav+1], results_amp[n_cav], 'k--')

plt.draw()

fig2 = plt.figure(2)
plt.title('Cavity Module - Phase Plot')
plt.xlabel('time (s)')
plt.ylabel('phase (deg)')
for i in range(n_cav):
    plt.plot(results_pha[n_cav+1], results_pha[i])

plt.plot(results_pha[n_cav+1], results_pha[n_cav], 'k--')
plt.show()

