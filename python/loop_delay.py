# 
#
# This module provide a loop delayer modeled as a FIFO list
#

import numpy as np

class LoopDelayer(object):
    def __init__(self, delay, step_time, initial_value = np.array([0.0, 0.0])):
        '''
            initialize the loop delayer.
            delay: time that is needed for the loop to run
            step: macro_time of the simulation
            initial_value: initial value of the loop
        '''
        
        if delay < step_time:
            raise ValueError(" delay must be >= step_time")

        self.delay_steps = int(delay/step_time) - 1
        self.fifo_queue = [initial_value] * self.delay_steps

    def push_pop(self, value):
        '''
            push back a value, pop front one
        '''
        self.fifo_queue.append(value)
        return self.fifo_queue.pop(0)
