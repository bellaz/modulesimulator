import math
import numpy as np
import random

def get_ff_fun(start_flattop, filling_ff, flattop_ff):
    '''
        get the feed forward table function
    '''
    def ff_(time):
        if time < start_flattop:
            return filling_ff
        else:
            return flattop_ff
    
    return ff_

def get_sp_fun(start_flattop, filling_sp, flattop_sp, cav_q, cav_freq): 
    '''
        get the set point table function
    '''
    def sp_(time):
        if time < start_flattop:
            return filling_sp * (1.0 - math.exp(-time * math.pi * cav_freq / cav_q))
        else:
            return flattop_sp
    
    return sp_

def get_measure_noise(std):
    '''
        get measure noise. A numpy 2-dimensional array randomly distribuited
        Gaussian distribution
    '''
    return np.array([random.gauss(0.0, std), random.gauss(0.0, std)])

def get_ff_ratio(start_flattop, cav_freq, cav_q):
    '''
        get the amplitude ratio between filling and flattop
    '''
    return (1.0 - math.exp(-start_flattop * math.pi * cav_freq / cav_q))

def get_input_from_desired_cavity_output(output_value, cav_q, cav_roq):
    '''
        get the input of the cavity giving the desired accelerating output strenght
    '''
    return output_value/(cav_q * cav_roq)

def get_output_from_desired_cavity_input(input_value, cav_q, cav_roq):
    '''
        get the output value from the input value
    '''
    return input_value * cav_q * cav_roq

def get_amp_angle_from_IQ(iq):
    '''
        get a tuple (amplitude, angle) from an iq value
    '''
    amp = math.sqrt(iq[0]*iq[0] + iq[1] * iq[1])
    angle = math.atan2(iq[1], iq[0])
    return (amp, angle)

def get_IQ_from_amp_angle(amp, angle):
    '''
        get an iq value from angle and amplitude
    '''
    return (amp * math.cos(angle), amp * math.sin(angle))

def get_ff_fun_from_cavity_parameters(desired_flattop_sp, start_flattop, cav_freq, cav_q, cav_roq):
    '''
        get the ff function from cavity parameters
    '''
    flattop_ff = get_input_from_desired_cavity_output(desired_flattop_sp, cav_q, cav_roq)
    amp_ratio = get_ff_ratio(start_flattop, cav_freq, cav_q)
    filling_ff = flattop_ff/amp_ratio
    return get_ff_fun(start_flattop, filling_ff, flattop_ff)

def get_sp_fun_from_cavity_parameters(desired_flattop_sp, start_flattop, cav_freq, cav_q, cav_roq):
    '''
        get the sp function from cavity parameters
    ''' 
    amp_ratio = get_ff_ratio(start_flattop, cav_freq, cav_q)
    filling_sp = desired_flattop_sp / amp_ratio
    return get_sp_fun(start_flattop, filling_sp, desired_flattop_sp, cav_q, cav_freq) 
