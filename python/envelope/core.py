'''
    This module provide a C++ implementation of the envelope simulator.. the
    methods are equal to the python one
'''
import module_simulator.core
from module_simulator.core import envelope as core_env
import module_simulator.envelope.envelope_factory as env_fac

class Factory(env_fac.Factory):
    '''
        Factory for the c++ implementation of the envelope simulator
    '''
    def to_module(self):
        '''
            produces a cavity module
        '''
        return core_env.Module(self)
