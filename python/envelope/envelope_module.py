'''
    envelope module. Module should be created only by a envelope_factory
'''
import module_simulator.envelope
from module_simulator.envelope import envelope_cavity

class Module(object):
    '''
        this class has the purpouse of simulating a superconducting module
        with various possible selectable effects. it should be created only by
        a factory
    '''
    
    def __init__(self, factory):
        '''
            produces a module from a factory
        '''
        self.cavities = []

        for cavity_params in factory.cavities:
            self.cavities.append(envelope_cavity.Cavity(factory.step_time,
                                        factory.source_frequency,
                                        cavity_params,
                                        factory.lfd,
                                        factory.beam_loading,
                                        factory.mechanical_resonances,
                                        factory.microphonic_sources))

    def macro_step(self, I, Q):
        '''
            perform a step in the simulation with IQ values
        '''
        return_IQs = []
        
        for cavity_sys in self.cavities:
            return_IQs.append(cavity_sys.macro_step(I,Q))

        return return_IQs

