'''
    types for the simulation
'''

from collections import namedtuple

CavityParams = namedtuple("CavityParams", "frequency Q RoQ initial_status")
MicrophonicParams = namedtuple("MicrophonicParams", "frequency phase amplitude")
MechanicalResonanceParams = namedtuple("MechanicalResonanceParams", "frequency Q coupling initial_status")
BeamLoadingParams = namedtuple("BeamLoadingParams", "bunch_charge bunch_separation bunch_phase start_time n_bunches")
