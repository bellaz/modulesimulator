'''
    This module provides an envelope simulator of a cavity
'''

import math
import numpy as np
from scipy.integrate import ode

class Cavity(object):
    '''
        This is the real simulator class. it performs the simulation step using
        a system of first order derivative functions. 
        Order of y:
        [V_I, V_Q, dw, dw1, dw2, dw3 ..., ddw1, ddw2, ddw3 ...].
        where V_I and V_Q are the output voltage. dw is the DYNAMIC detuning.
        dwn(s) are the detunings produced by mechanical resonances, and ddwn are
        their derivatives
    '''

    def __init__(self,
            step_time,
            source_frequency,
            cavity_params,
            lfd,
            beam_loading,
            mechanical_resonances,
            microphonic_sources):
        '''
            Initialize the module and precompute the equations system and its
            jacobian
        '''

        self.step_time = step_time
        self.source_frequency = source_frequency
        self.cavity_params = cavity_params
        self.lfd = 2.0 * math.pi * lfd
        self.beam_loading = beam_loading
        self.mechanical_resonances = mechanical_resonances
        self.microphonic_sources = microphonic_sources

        self.RL = self.cavity_params.Q * self.cavity_params.RoQ
        self.initial_detuning = 2.0 * math.pi * (self.source_frequency - self.cavity_params.frequency)
        self.half_bw = math.pi * self.cavity_params.frequency / self.cavity_params.Q
        #self.dwmps = [2.0 * math.pi * mi_source.amplitude for m_source in self.microphonic_sources]
        #self.wmps = [2.0 * math.pi * mi_source.frequency for mi_source in self.microphonic_sources]

        self.wmos = [2.0 * math.pi * me_resonance.frequency for me_resonance in self.mechanical_resonances]
        self.minus_squared_wmos = [-wmo * wmo for wmo in self.wmos]
        self.minus_squared_wmos_4_mpi_ks = [2.0 * math.pi * me_res.coupling * m_s_wmo
                for (m_s_wmo, me_res) 
                in zip(self.minus_squared_wmos, self.mechanical_resonances)]

        self.minus_wmo_over_Q = [ - 2.0 * math.pi * me_res.frequency / me_res.Q for me_res in self.mechanical_resonances]

        self.MECHANICAL_RESONANCES = len(self.mechanical_resonances)
        self.SYSTEM_SIZE = self.MECHANICAL_RESONANCES * 2 + 3

        self.beam_loading_current = 0.0
        self.system_eq = np.zeros(self.MECHANICAL_RESONANCES)
        self.system_jac = np.zeros((self.MECHANICAL_RESONANCES, self.MECHANICAL_RESONANCES))


        self.jac_template = np.zeros((self.SYSTEM_SIZE, self.SYSTEM_SIZE))

        self.initial_status = np.zeros(self.SYSTEM_SIZE)
        self.initial_status[0] = self.cavity_params.initial_status[0]
        self.initial_status[1] = self.cavity_params.initial_status[1]
        self.initial_status[2] = self.initial_detuning # must see if this or negative

        for i in range(self.MECHANICAL_RESONANCES):
            
            self.initial_status[i + 3] = 2.0 * math.pi * self.mechanical_resonances[i].initial_status[0]
            self.initial_status[i + 3 + self.MECHANICAL_RESONANCES] = 2.0 * math.pi * self.mechanical_resonances[i].initial_status[1]

            self.jac_template[2][i + 3 + self.MECHANICAL_RESONANCES] = 1
            self.jac_template[i + 3][i + 3 + self.MECHANICAL_RESONANCES] = 1
            self.jac_template[i + 3 + self.MECHANICAL_RESONANCES][0] = 2.0 * self.minus_squared_wmos_4_mpi_ks[i]
            self.jac_template[i + 3 + self.MECHANICAL_RESONANCES][1] = 2.0 * self.minus_squared_wmos_4_mpi_ks[i]
            self.jac_template[i + 3 + self.MECHANICAL_RESONANCES][i + 3] = self.minus_squared_wmos[i]
            self.jac_template[i + 3 + self.MECHANICAL_RESONANCES][i + 3 + self.MECHANICAL_RESONANCES] = self.minus_wmo_over_Q[i]
            

        self.step = 0
        self.bl_start_step = int(self.beam_loading.start_time/self.step_time)
        current_abs = self.beam_loading.bunch_charge / self.step_time 
        self.peak_current = (current_abs * math.cos(self.beam_loading.bunch_phase), current_abs * math.sin(self.beam_loading.bunch_phase))
        self.integrator = ode(f,jac).set_integrator('dopri5')
        self.integrator.set_initial_value(self.initial_status, 0.0)
        self.integrator.set_f_params(self)
        self.integrator.set_jac_params(self)

    def macro_step(self, I, Q):
        '''
            perform a computation (macro step)
        '''
        self.I = I
        self.Q = Q
        self.compute_bl()
        result = self.integrator.integrate(self.integrator.t + self.step_time)
        self.step = self.step + 1
        return (result[0], result[1])

    def compute_bl(self):
        '''
            compute beam loading current
        '''
        if ((self.step >= self.bl_start_step) 
                and (((self.step - self.bl_start_step) % (self.beam_loading.bunch_separation + 1)) == 0)
                and ((self.beam_loading.n_bunches < 0) or ((self.step - self.bl_start_step) / (self.beam_loading.bunch_separation + 1) < self.beam_loading.n_bunches))):
            self.I += self.peak_current[0]
            self.Q += self.peak_current[1]

def f(t, y, cavity):
    '''
        system function
    '''
    VI = y[0]
    VQ = y[1]
    dw = y[2]

    II = cavity.I
    IQ = cavity.Q
    k = cavity.lfd
    w12 = cavity.half_bw
    RL = cavity.RL
    RLw12 = RL * w12

    squared_V = (VI * VI + VQ * VQ)
    dy = np.zeros(cavity.SYSTEM_SIZE)

    dw_eff = (dw - k * squared_V) 

    dy[0] = -w12 * VI - dw_eff * VQ + RLw12 * II
    dy[1] = dw_eff * VI - w12 * VQ + RLw12 * IQ
    for i in range(cavity.MECHANICAL_RESONANCES):
        dy[2] += y[3 + cavity.MECHANICAL_RESONANCES + i]
    
    for m_src in cavity.microphonic_sources:
        dy[2] += (-4.0 * math.pi * math.pi * m_src.amplitude * m_src.frequency * 
                    math.cos(2.0 * math.pi * m_src.frequency * t + m_src.phase)) 

    for i in range(cavity.MECHANICAL_RESONANCES):
        dy[3 + i] = y[3 + cavity.MECHANICAL_RESONANCES + i]
        dy[3 + cavity.MECHANICAL_RESONANCES + i] = (cavity.minus_squared_wmos[i] * y[3 + i]
                                                    + cavity.minus_wmo_over_Q[i] * y[3 + cavity.MECHANICAL_RESONANCES + i]
                                                    + cavity.minus_squared_wmos_4_mpi_ks[i] * squared_V)

    return dy

def jac(t, y, cavity):
    '''
        jacobian function
    '''
    VI = y[0]
    VQ = y[1]
    dw = y[2]

    k = cavity.lfd
    w12 = cavity.half_bw
    RL = cavity.RL

    VIVQk2 = 2 * VI * VQ * k

    jacm = np.copy(cavity.jac_template)
    jacm[0][0] =  VIVQk2 - w12
    jacm[0][1] =  k * ( 3.0 * VQ * VQ + VI * VI) - dw
    jacm[1][0] = -k * ( VQ * VQ + 3.0 * VI * VI) + dw
    jacm[1][1] = -w12 - VIVQk2
    jacm[0][2] = -VQ
    jacm[1][2] = VI
    for i in range(self.MECHANICAL_RESONANCES):
        jacm[3 + self.MECHANICAL_RESONANCES + i][0] *= VI
        jacm[3 + self.MECHANICAL_RESONANCES + i][1] *= VQ

    return jacm
