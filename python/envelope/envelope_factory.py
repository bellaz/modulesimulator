'''
    this module provides Factory. this class is used to produce modules of cavities
'''
from module_simulator.envelope.envelope_types import *
from module_simulator.envelope.envelope_module import Module

class Factory(object):
    '''
        This object produces cavity modules. 
    '''
    def __init__(self, step_time, source_frequency):
        '''
            Initialize the factory with the step time
        '''
        
        self.step_time = step_time
        self.source_frequency = source_frequency
        self.cavities = []
        self.lfd = 0.0
        self.microphonic_sources = []
        self.beam_loading = BeamLoadingParams(0.0, 0, 0.0, 0.0, 0)
        self.mechanical_resonances = []

    def add_cavity(self, frequency, Q, RoQ, initial_status = (0.0, 0.0)):
        '''
            Add a cavity with its frequency Q and RoQ
        '''
        self.cavities.append(CavityParams(frequency, Q, RoQ, initial_status))

    def delete_cavities(self):
        '''
            Delete registered cavities
        '''
        self.cavities = []

    def set_lfd(self, lfd):
        '''
            Set the static detuning
        '''
        self.lfd = lfd

    def delete_lfd(self):
        '''
            Set the static detuning to 0
        '''
        self.lfd = 0.0

    def add_mechanical_resonance(self, frequency, Q, coupling, initial_status = (0.0, 0.0)):
        '''
            add a mechanical resonance
        '''
        self.mechanical_resonances.append(MechanicalResonanceParams(frequency, Q, coupling, initial_status))

    def delete_mechanical_resonance(self):
        '''
            delete mechanical resonances
        '''
        self.mechanical_resonances = []

    def set_beam_loading(self, bunch_charge, bunch_separation, bunch_phase, start_time, n_bunches = -1):
        '''
            Set the beam loading. if n_bunches is negative then the number of bunches are infinite
        '''
        self.beam_loading = BeamLoadingParams(bunch_charge,
                                              bunch_separation,
                                              bunch_phase,
                                              start_time,
                                              n_bunches)

    def delete_beam_loading(self):
        '''
            delete beam loading
        '''
        self.beam_loading = BeamLoadingParams(0.0, 0, 0.0, 0.0, 0)
            

    def add_microphonic_source(self, frequency, phase, amplitude):
        '''
            add a microphonic source
        '''
        self.microphonic_sources.append(MicrophonicParams(frequency, phase, amplitude))

    def to_module(self):
        '''
            produces a cavity module
        '''
        return Module(self) 
    
