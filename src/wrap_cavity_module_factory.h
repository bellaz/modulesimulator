// This module provides a wrap for boost python of cavity module factory

#pragma once
#include<boost/python.hpp>
#include "cavity_module_factory.h"

class WrapCavityModuleFactory;

#include "wrap_cavity_module.h"

using namespace boost::python;

class WrapCavityModuleFactory : public CavityModuleFactory {
    public:
        WrapCavityModuleFactory(double timestep, unsigned int int_steps);    
        WrapCavityModuleFactory(CavityModuleFactory& cavity_factory);
        str wrap_serialize() const;
        static WrapCavityModuleFactory wrap_deserialize(const str& serialized_factory);
        WrapCavityModule wrap_to_module() const;
};
