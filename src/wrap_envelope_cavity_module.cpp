#include<cmath>
#include "wrap_envelope_cavity_module.h"

EnvCavityModule from_factory(const object& factory){

    double step_time = extract<double>(factory.attr("step_time"));
    double source_frequency = extract<double>(factory.attr("source_frequency")); 

    std::vector<CavityParams> cavities_params;
   
     
    for(auto cav_it = stl_input_iterator<boost::python::object>(factory.attr("cavities"));
        cav_it != stl_input_iterator<boost::python::object>();
        cav_it++) {
        
        CavityParams cavity_params;
        cavity_params.frequency = extract<double>((*cav_it).attr("frequency"));
        cavity_params.Q = extract<double>((*cav_it).attr("Q"));
        cavity_params.RoQ = extract<double>((*cav_it).attr("RoQ"));
        cavity_params.iniVI = extract<double>((*cav_it).attr("initial_status")[0]);
        cavity_params.iniVQ = extract<double>((*cav_it).attr("initial_status")[1]);
        cavities_params.push_back(cavity_params);
    }

    double lfd = extract<double>(factory.attr("lfd"));

    object bl_params_ = factory.attr("beam_loading");
    BeamLoadingParams bl_params;
    bl_params.bunch_charge = extract<double>(bl_params_.attr("bunch_charge"));
    bl_params.bunch_separation = extract<unsigned int>(bl_params_.attr("bunch_separation"));
    bl_params.bunch_phase = extract<double>(bl_params_.attr("bunch_phase"));
    bl_params.start_time = extract<double>(bl_params_.attr("start_time"));
    bl_params.n_bunches = extract<int>(bl_params_.attr("n_bunches"));

    std::vector<MechanicalResonanceParams> mech_res_params;
    
    for(auto res_it = stl_input_iterator<boost::python::object>(factory.attr("mechanical_resonances"));
        res_it != stl_input_iterator<boost::python::object>();
        res_it++) {

        MechanicalResonanceParams mech_re_params;
        mech_re_params.frequency = extract<double>((*res_it).attr("frequency"));
        mech_re_params.Q = extract<double>((*res_it).attr("Q"));
        mech_re_params.coupling = extract<double>((*res_it).attr("coupling"));
        mech_re_params.iniw  = 2.0 * M_PI * extract<double>((*res_it).attr("initial_status")[0]);
        mech_re_params.inidw = 2.0 * M_PI * extract<double>((*res_it).attr("initial_status")[1]);
        mech_res_params.push_back(mech_re_params);
    }
    
    std::vector<MicrophonicParams> micros_params;

    for(auto micro_it = stl_input_iterator<boost::python::object>(factory.attr("microphonic_sources"));
        micro_it != stl_input_iterator<boost::python::object>();
        micro_it++) {
        
        MicrophonicParams micro_param;
        micro_param.frequency = extract<double>((*micro_it).attr("frequency"));
        micro_param.phase = extract<double>((*micro_it).attr("phase"));
        micro_param.amplitude = extract<double>((*micro_it).attr("amplitude"));
        micros_params.push_back(micro_param);
        }

    return EnvCavityModule(step_time,
            source_frequency, 
            cavities_params,
            lfd, 
            bl_params, 
            mech_res_params,
            micros_params);   
};

WrapEnvCavityModule::WrapEnvCavityModule(const object& factory) : EnvCavityModule(from_factory(factory)) {}; 

list WrapEnvCavityModule::wrapped_macro_step(const double& I, const double& Q){

    list iq_return;
    std::vector<IQValue> iq_return_;
    iq_return_ = this->macro_step(IQValue(I, Q));

    for(const IQValue& iq_ret_value : iq_return_){

        boost::python::tuple iq_ret_value_ = boost::python::make_tuple<double, double>(iq_ret_value.I, iq_ret_value.Q);
        iq_return.append(iq_ret_value_);
    }

    return iq_return;

};
