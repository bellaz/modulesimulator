/*
 * Module to modelize Lorentz force detuning. Naive implementation of a Lorentz force 
 * detuning
 */

#pragma once
#include<jsoncpp/json/json.h>
#include<vector>
#include "ideal_cavity.h"

#define CLS_LORENTZ_FORCE_DETUNING "LorentzForceDetuning"

typedef IdealCavity MechanicalResonance;

class LorentzForceDetuning {

    public:
        LorentzForceDetuning(double timestep = 1.0, double k_lfd = 0.0);
        void step(double V);
        double get_frequency_delta() const;
        double get_k_lfd() const;
        void add_mechanical_resonance(double frequency, double Q, double coupling);
        void delete_mechanical_resonances(); 
        void set_k_lfd(double k_lfd);
        bool operator==(const LorentzForceDetuning& other) const;
        Json::Value serialize() const;
        static LorentzForceDetuning deserialize(const Json::Value& serialized_lfd);
    private:
        
        double timestep;
        double frequency;
        double k_lfd;
        double delta_frequency;
        double V0, Vm1;
        double frequency_k_lfd;
        std::vector<MechanicalResonance> mechanical_resonances;
};
