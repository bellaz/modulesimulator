/*
 *
 * This represents a cavity module that keeps runnable cavities. It is exposed 
 * to python through its interface
 */

#pragma once
#include<string>
#include<jsoncpp/json/json.h>
#include "module_basic_exception.h"
#include "cavity_module.h"
#include "cavity.h"
#include "ideal_cavity.h"
#include "discrete_oscillator.h"
#include "lfd.h"
#include "iq_value.h"

#define CLS_CAVITY_MODULE_FACTORY "CavityModuleFactory"

using namespace std;

class CavityModule;

class CavityModuleFactory{

    friend class CavityModule;

    public:

    CavityModuleFactory(double timestep = 1.0, unsigned int_steps = 0.01);

    void add_cavity(double frequency = 1.0, double Q = 100.0, double RoQ = 1.0);

    void delete_cavities();

    void add_power_source(double frequency);

    void delete_power_sources();

    void set_lfd(double k_lfd);

    void delete_lfd();

    void add_mechanical_resonance(double frequency, double Q, double coupling);

    void delete_mechanical_resonances();

    void set_beam_loading(double baseband_frequency, double bunch_charge,
            unsigned int bunch_separation, double bunch_phase, double start_time,
            int n_bunches);
    
    void delete_beam_loading();
/// parameters looks a bit strange...
    void add_microphonic_source(double frequency, double phase, double freq_amplitude);

    void delete_microphonic_sources();

    void set_power_source(unsigned int cavity, unsigned int ref_power_source);

    bool operator==(const CavityModuleFactory& other) const;

    CavityModule to_cavity_module() const;
    Json::Value serialize() const;
    static CavityModuleFactory deserialize(const Json::Value& serialized_cavity);


    private:

    unsigned int int_steps;
    double timestep;
    vector<Cavity> cavities;
    LorentzForceDetuning lfd;
    vector<DiscreteOscillator> power_sources;
    vector<DiscreteOscillator> micro_sources;
    BeamLoading beam_loading;
    
};

class CavityIndexBoundaryException : ModuleException {

    public:
    CavityIndexBoundaryException(unsigned int index, unsigned int boundary) : index(index), boundary(boundary) {};

    char const* what() throw() {
        string err_str;
        err_str += "cavity index selected: ";
        err_str += this->index;
        err_str += " . number of cavities: ";
        err_str += this->boundary;

        return err_str.c_str();
    };

    private:

    unsigned int index, boundary;
};


class SourceIndexBoundaryException : ModuleException {
    public:
    SourceIndexBoundaryException(unsigned int index, unsigned int boundary) : index(index), boundary(boundary) {};
    char const* what() throw() {
     
        string err_str;
        err_str += "source index selected: ";
        err_str += this->index;
        err_str += " . number of cavities: ";
        err_str += this->boundary;

        return err_str.c_str();
    };

    private:

    unsigned int index, boundary;
};

class NoCavitiesException : ModuleException {
    public:
    char const* what() throw() { return "At least add a cavity before building a cavity module";}; 
};
