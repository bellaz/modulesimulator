
#include "beam_loading.h"
#include<cmath>
#include<string>

#define _USE_MATH_DEFINES
#define CLS_BEAM_LOADING "BeamLoading"

using namespace std;

BeamLoading::BeamLoading(double timestep, double baseband_frequency, double bunch_charge, unsigned int bunch_separation, double bunch_phase, double start_time, int n_bunches, double baseband_phase):
    timestep(timestep), baseband_frequency(baseband_frequency), bunch_charge(bunch_charge),
    bunch_separation(bunch_separation), bunch_phase(bunch_phase), start_time(start_time),
    n_bunches(n_bunches), baseband_phase(baseband_phase){

        this->out_current = 0.0; 
        this->step_current = this->bunch_charge / this->timestep;
        this->time_separation = this->bunch_separation / this->baseband_frequency;
        this->time = this->timestep * 0.5;
        this->real_start = (this->baseband_phase + this->bunch_phase)/(2 * M_PI * this->baseband_frequency);
        this->real_start += floor(this->start_time * this->baseband_frequency)/this->baseband_frequency;
        this->bunches_to_do = this->n_bunches;
    };

bool BeamLoading::operator==(const BeamLoading& other) const {

    return (this->timestep        == other.timestep) &&
        (this->baseband_frequency == other.baseband_frequency) &&
        (this->bunch_charge       == other.bunch_charge) &&
        (this->bunch_separation   == other.bunch_separation) &&
        (this->bunch_phase        == other.bunch_phase) &&
        (this->start_time         == other.start_time) &&
        (this->n_bunches          == other.n_bunches) &&
        (this->baseband_phase     == other.baseband_phase) &&
        (this->out_current        == other.out_current) &&
        (this->step_current       == other.step_current) &&
        (this->time_separation    == other.time_separation)  &&
        (this->time               == other.time) &&
        (this->real_start         == other.real_start) &&
        (this->bunches_to_do      == other.bunches_to_do);
};

void BeamLoading::reset(){

    this->time = this->timestep * 0.5;
    this->bunches_to_do = this->n_bunches;
    this->out_current = 0.0; 
}

Json::Value BeamLoading::serialize() const {
    Json::Value serialized_bl;

    serialized_bl["__cls"]              = string(CLS_BEAM_LOADING);
    serialized_bl["timestep"]           = this->timestep;
    serialized_bl["baseband_frequency"] = this->baseband_frequency;
    serialized_bl["bunch_charge"]       = this->bunch_charge;
    serialized_bl["bunch_separation"]   = this->bunch_separation;
    serialized_bl["bunch_phase"]        = this->bunch_phase;
    serialized_bl["start_time"]         = this->start_time;
    serialized_bl["n_bunches"]          = this->n_bunches;
    serialized_bl["baseband_phase"]     = this->baseband_phase;
    serialized_bl["out_current"]        = this->out_current;
    serialized_bl["step_current"]       = this->step_current;
    serialized_bl["time_separation"]    = this->time_separation;
    serialized_bl["time"]               = this->time;
    serialized_bl["real_start"]         = this->real_start;
    serialized_bl["bunches_to_do"]      = this->bunches_to_do;
    
    return serialized_bl;
};

BeamLoading BeamLoading::deserialize(const Json::Value& serialized_bl){

BeamLoading deserialized_bl;
                            
    deserialized_bl.timestep = serialized_bl["timestep"].asDouble();
    deserialized_bl.baseband_frequency = serialized_bl["baseband_frequency"].asDouble();
    deserialized_bl.bunch_charge = serialized_bl["bunch_charge"].asDouble();
    deserialized_bl.bunch_separation = serialized_bl["bunch_separation"].asUInt();
    deserialized_bl.bunch_phase = serialized_bl["bunch_phase"].asDouble();
    deserialized_bl.start_time = serialized_bl["start_time"].asDouble();
    deserialized_bl.n_bunches = serialized_bl["n_bunches"].asInt();
    deserialized_bl.baseband_phase = serialized_bl["baseband_phase"].asDouble();
    deserialized_bl.out_current = serialized_bl["out_current"].asDouble();
    deserialized_bl.step_current = serialized_bl["step_current"].asDouble();
    deserialized_bl.time_separation = serialized_bl["time_separation"].asDouble();
    deserialized_bl.time = serialized_bl["time"].asDouble();
    deserialized_bl.real_start = serialized_bl["real_start"].asDouble();
    deserialized_bl.bunches_to_do = serialized_bl["bunches_to_do"].asInt();

    return deserialized_bl;
};

