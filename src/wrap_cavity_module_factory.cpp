#include "wrap_cavity_module_factory.h"


WrapCavityModuleFactory::WrapCavityModuleFactory(double timestep, unsigned int int_steps) :
    CavityModuleFactory(timestep, int_steps) {};    

WrapCavityModuleFactory::WrapCavityModuleFactory(CavityModuleFactory& factory) : CavityModuleFactory(factory) {};

str WrapCavityModuleFactory::wrap_serialize() const {
 
    Json::Value serialized_json = this->CavityModuleFactory::serialize();
    Json::StyledWriter writer;
    return str(writer.write(serialized_json));   
};

WrapCavityModuleFactory WrapCavityModuleFactory::wrap_deserialize(const str& serialized_factory) {

    Json::Reader reader;
    Json::Value  parsed;
    const char* raw_serialized = extract<const char*>(serialized_factory);
    std::string serialized_factory_(raw_serialized);
    reader.parse(serialized_factory_, parsed);
    CavityModuleFactory cavity_factory = CavityModuleFactory::deserialize(parsed);
    return WrapCavityModuleFactory(cavity_factory);

};

WrapCavityModule WrapCavityModuleFactory::wrap_to_module() const {

    CavityModule module = this->CavityModuleFactory::to_cavity_module(); 
    return WrapCavityModule(module);
};
