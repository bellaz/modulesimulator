#include<boost/python.hpp>
#include "wrap_cavity_module.h"
#include "wrap_cavity_module_factory.h"
#include "wrap_envelope_cavity_module.h"

class Envelope {};

void translate(ModuleException const& e) {

    PyErr_SetString(PyExc_RuntimeError, e.what());
}

BOOST_PYTHON_MODULE(core) {
    using namespace boost::python;
   
    register_exception_translator<ModuleException>(&translate);

    class_<WrapCavityModule>("Module", no_init)
        .def("macro_step", &WrapCavityModule::wrapped_macro_step)
        .def("serialize", &WrapCavityModule::wrapped_serialize)
        .def("deserialize", &WrapCavityModule::wrapped_deserialize)
        .staticmethod("deserialize")
        .def("to_factory", &WrapCavityModule::wrapped_to_factory);

    class_<WrapCavityModuleFactory>("Factory", init<double, unsigned int>())
        .def("add_cavity", &WrapCavityModuleFactory::add_cavity)
        .def("delete_cavities", &WrapCavityModuleFactory::delete_cavities)
        .def("add_power_source", &WrapCavityModuleFactory::add_power_source)
        .def("delete_power_sources", &WrapCavityModuleFactory::delete_power_sources)
        .def("set_lfd", &WrapCavityModuleFactory::set_lfd)
        .def("delete_lfd", &WrapCavityModuleFactory::delete_lfd)
        .def("set_beam_loading", &WrapCavityModuleFactory::set_beam_loading)
        .def("add_mechanical_resonance", &WrapCavityModuleFactory::add_mechanical_resonance)
        .def("delete_mechanical_resonances", &WrapCavityModuleFactory::delete_mechanical_resonances)
        .def("delete_beam_loading", &WrapCavityModuleFactory::delete_beam_loading)
        .def("add_microphonic_source", &WrapCavityModuleFactory::add_microphonic_source)
        .def("delete_microphonic_sources", &WrapCavityModuleFactory::delete_power_sources)
        .def("set_power_source", &WrapCavityModuleFactory::set_power_source)
        .def("serialize", &WrapCavityModuleFactory::wrap_serialize)
        .def("deserialize", &WrapCavityModuleFactory::wrap_deserialize)
        .staticmethod("deserialize")
        .def("to_module", &WrapCavityModuleFactory::wrap_to_module);

    
    scope envelope = class_<Envelope>("envelope");
    class_<WrapEnvCavityModule>("Module", init<object>())
        .def("macro_step", &WrapEnvCavityModule::wrapped_macro_step);
}
