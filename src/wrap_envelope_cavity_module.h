/*
 * Wrap module of envelope class for python
 *
 */
#pragma once
#include<boost/python.hpp>

#include "envelope_cavity_module.h"

using namespace boost::python;

class WrapEnvCavityModule : public EnvCavityModule {
    public:
        WrapEnvCavityModule(const object& factory); 
        list wrapped_macro_step(const double &I, const double &Q);
};
