/*
 * Types for native envelope simulator
 */

#pragma once

typedef struct {
    double frequency;
    double Q;
    double RoQ;
    double iniVI, iniVQ;
} CavityParams;

typedef struct {
    double frequency;
    double phase;
    double amplitude;
} MicrophonicParams;

typedef struct {
    double frequency;
    double Q;
    double coupling;
    double iniw, inidw;
} MechanicalResonanceParams;

typedef struct {
    double bunch_charge;
    unsigned int bunch_separation;
    double bunch_phase;
    double start_time;
    int n_bunches;
} BeamLoadingParams;
    


