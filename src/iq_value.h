/*
 * This module provide IQ value class with nice helpers
 */

#pragma once
#include<jsoncpp/json/json.h>
#include<cmath>

#define CLS_IQ_VALUE "IQValue"

using namespace std;

class IQValue{
    public:

        IQValue(double I, double Q){
            this->I = I;
            this->Q = Q;
        };

        IQValue(const IQValue& other){
            this->I = other.I;
            this->Q = other.Q;
        }
        
        IQValue(IQValue&& other){
            this->I = other.I;
            this->Q = other.Q;
        }

        void operator=(const IQValue& other){
            this->I = other.I;
            this->Q = other.Q;
        }
        
        void operator=(IQValue&& other){
            this->I = other.I;
            this->Q = other.Q;
        }

        double angle() const {
            return atan2(this->Q, this->I);
        };
        
        double norm() const {
            return sqrt(this->I * this->I + this->Q * this->Q);
        };

        bool operator==(const IQValue& other) const {
            return (this->I == other.I) and (this->Q == other.Q);
        };
        
        bool operator!=(const IQValue& other) const {
            return (this->I != other.I) or (this->Q != other.Q);
        };

        IQValue operator+(const IQValue& other) const {
            return IQValue(this->I + other.I, this->Q + other.Q);   
        };

        IQValue operator-(const IQValue& other) const {
            return IQValue(this->I - other.I, this->Q - other.Q);   
        };

        IQValue operator*(const double value) const {
            return IQValue(this->I * value, this->Q * value);
        };

        IQValue operator*(const IQValue& other) const {
             return IQValue(this->I * other.I, this->Q * other.Q);
        };

        void operator+=(const IQValue& other){
            
            this->I += other.I;
            this->Q += other.Q;
        };

        void operator-=(const IQValue& other){
            
            this->I -= other.I;
            this->Q -= other.Q;
        };

        void operator*=(double value){
            
            this->I *= value;
            this->Q *= value;
        };

        double scalar(const IQValue& other) const {
            return this->I * other.I + this->Q * other.Q;
        };

        Json::Value serialize() const;
        static IQValue deserialize(const Json::Value&);

    double I;
    double Q;
};
