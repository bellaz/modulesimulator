/*
 * provide native envelope simulator module
 *
 */

#pragma once
#include<vector>
#include "iq_value.h"
#include "envelope_cavity.h"


class EnvCavityModule {

    public:

    EnvCavityModule(double step_time,
            double source_frequency, 
            std::vector<CavityParams> cavities_params,
            double lfd, 
            BeamLoadingParams bl_params, 
            std::vector<MechanicalResonanceParams> mech_res_params,
            std::vector<MicrophonicParams> micro_params);


    vector<IQValue> macro_step(IQValue current);

    private:
        vector<EnvCavity> cavities;
};
