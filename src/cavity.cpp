#include<algorithm>
#include "cavity.h"
#include<iostream>

using namespace std;

Cavity::Cavity(unsigned int int_steps, double timestep, double frequency, double Q, double RoQ, IQValue initial_state) :
    int_steps(int_steps), ideal_cavity(IdealCavity(timestep, frequency, Q, RoQ, initial_state)),
    power_sources(vector<DiscreteOscillator>()), ref_power_source(0), lfd(LorentzForceDetuning()),
    beam_loading(BeamLoading()), micro_sources(vector<DiscreteOscillator>()),
    V(IQValue(0.0, 0.0)), mean_V(0.0) {};

void Cavity::add_power_source(const DiscreteOscillator& power_source){
    
    this->power_sources.push_back(power_source);
};

void Cavity::delete_power_sources(){
    
    this->power_sources.clear();
};

void Cavity::set_lfd(const LorentzForceDetuning& lfd){

    this->lfd = lfd;
};

void Cavity::delete_lfd(){

    this->lfd = LorentzForceDetuning();
};

void Cavity::set_beam_loading(const BeamLoading& bl){

    this->beam_loading = bl;
};

void Cavity::delete_beam_loading(){

    this->beam_loading = BeamLoading();
};

void Cavity::add_microphonic_source(const DiscreteOscillator& micro_source){
    
    this->micro_sources.push_back(micro_source);
};

void Cavity::delete_microphonic_sources(){
    
    this->micro_sources.clear();
};

void Cavity::set_power_source(unsigned int ref_power_source){

    this->ref_power_source = ref_power_source;
};

unsigned int Cavity::get_power_source() const {
    return this->ref_power_source;
};

unsigned int Cavity::get_n_sources() const {

    return this->power_sources.size();
};

double Cavity::get_resonant_frequency() const{
    return this->ideal_cavity.get_resonant_frequency();   
};


double Cavity::get_timestep() const {
    return this->ideal_cavity.get_timestep();
};

unsigned int Cavity::get_int_steps() const {
    return this->int_steps;
};

bool Cavity::operator==(const Cavity& other) const {
 return (this->int_steps == other.int_steps) &&
        (this->ideal_cavity == other.ideal_cavity) &&
        (std::equal(this->power_sources.begin(), this->power_sources.end(), other.power_sources.begin())) &&
        (this->ref_power_source == other.ref_power_source) &&
        (this->lfd == other.lfd) &&
        (this->beam_loading == this->beam_loading) &&
        (std::equal(this->micro_sources.begin(), this->micro_sources.end(), other.micro_sources.begin())) &&
        (this->V == other.V) &&
        (this->mean_V == other.mean_V);

};

Json::Value Cavity::serialize() const {
    
    Json::Value serialized_cavity;
    serialized_cavity["__cls"] = CLS_CAVITY;
    serialized_cavity["int_steps"] = this->int_steps;
    serialized_cavity["ideal_cavity"] = this->ideal_cavity.serialize();
    
    Json::Value serialized_power_sources(Json::arrayValue);
    for(vector<DiscreteOscillator>::const_iterator it = this->power_sources.begin(); it != this->power_sources.end(); it++){

        serialized_power_sources.append((*it).serialize());
    }

    serialized_cavity["power_sources"] = serialized_power_sources; 
    serialized_cavity["ref_power_source"] = this->ref_power_source;
    serialized_cavity["lfd"] = this->lfd.serialize();
    serialized_cavity["beam_loading"] = this->beam_loading.serialize();
    
    Json::Value serialized_micro_sources(Json::arrayValue);
    for(vector<DiscreteOscillator>::const_iterator it = this->micro_sources.begin(); it != this->micro_sources.end(); it++){

        serialized_micro_sources.append((*it).serialize());
    }
    
    serialized_cavity["micro_sources"] = serialized_micro_sources;
    serialized_cavity["V"] = this->V.serialize();
    serialized_cavity["Vmean"] = this->mean_V;

    return serialized_cavity;
};

Cavity Cavity::deserialize(const Json::Value& serialized_cavity){

    Cavity deserialized_cavity;
    deserialized_cavity.int_steps = serialized_cavity["int_steps"].asUInt();
    deserialized_cavity.ideal_cavity = IdealCavity::deserialize(serialized_cavity["ideal_cavity"]);
    for(Json::Value::const_iterator it = serialized_cavity["power_sources"].begin();
         it != serialized_cavity["power_sources"].end();
         it++){

        deserialized_cavity.add_power_source(DiscreteOscillator::deserialize(*it));
    }

    deserialized_cavity.ref_power_source = serialized_cavity["ref_power_source"].asUInt();
    deserialized_cavity.lfd = LorentzForceDetuning::deserialize(serialized_cavity["lfd"]); 
    deserialized_cavity.beam_loading = BeamLoading::deserialize(serialized_cavity["beam_loading"]);
    for(Json::Value::const_iterator it = serialized_cavity["micro_sources"].begin();
         it != serialized_cavity["micro_sources"].end();
         it++){

        deserialized_cavity.add_microphonic_source(DiscreteOscillator::deserialize(*it));
    }
    
    deserialized_cavity.V = IQValue::deserialize(serialized_cavity["V"]);
    deserialized_cavity.mean_V = serialized_cavity["Vmean"].asDouble();
    
    return deserialized_cavity;
};
