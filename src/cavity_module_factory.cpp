#include "cavity_module_factory.h"
#include<omp.h>

CavityModuleFactory::CavityModuleFactory(double timestep, unsigned int int_steps) :
            int_steps(int_steps), timestep(timestep), cavities(vector<Cavity>()),
            lfd(timestep*int_steps, 0.0), power_sources(vector<DiscreteOscillator>()),
            micro_sources(vector<DiscreteOscillator>()), beam_loading(BeamLoading()) {};

void CavityModuleFactory::add_cavity(double frequency, double Q, double RoQ){
    this->cavities.push_back(Cavity(this->int_steps, this->timestep, frequency, Q, RoQ));   
};

void CavityModuleFactory::delete_cavities(){
    this->cavities.clear();   
};

void CavityModuleFactory::add_power_source(double frequency){
    this->power_sources.push_back(DiscreteOscillator(this->timestep, frequency));   
};

void CavityModuleFactory::delete_power_sources(){
    this->power_sources.clear();
};

void CavityModuleFactory::set_lfd(double k_lfd){
    this->lfd.set_k_lfd(k_lfd);
};

void CavityModuleFactory::delete_lfd(){
    this->lfd.set_k_lfd(0.0);    
};

void CavityModuleFactory::add_mechanical_resonance(double frequency, double Q, double coupling){
    
    this->lfd.add_mechanical_resonance(frequency, Q, coupling);
};

void CavityModuleFactory::delete_mechanical_resonances(){

    this->lfd.delete_mechanical_resonances();
};

void CavityModuleFactory::set_beam_loading(double baseband_frequency, double bunch_charge,
        unsigned int bunch_separation, double bunch_phase, double start_time,
        int n_bunches){
    this->beam_loading = BeamLoading(this->timestep, baseband_frequency, bunch_charge,
            bunch_separation, bunch_phase, start_time, n_bunches);
};

void CavityModuleFactory::delete_beam_loading(){
    this->beam_loading = BeamLoading();
};

void CavityModuleFactory::add_microphonic_source(double frequency, double phase, double freq_amplitude){
    this->micro_sources.push_back(DiscreteOscillator(this->timestep * this->int_steps, frequency, phase, freq_amplitude));   
};

void CavityModuleFactory::delete_microphonic_sources(){
    this->micro_sources.clear();   
};

void CavityModuleFactory::set_power_source(unsigned int cavity, unsigned int ref_power_source){
    
    if(cavity >= this->cavities.size()){
        throw CavityIndexBoundaryException(cavity, this->cavities.size());
    };    
    
    this->cavities[cavity].set_power_source(ref_power_source);
};


CavityModule CavityModuleFactory::to_cavity_module() const{
    CavityModule module;

    if (this->cavities.size() == 0){
        throw NoCavitiesException();
    }

    for(vector<Cavity>::const_iterator cav = this->cavities.begin(); cav != this->cavities.end(); cav++){
        Cavity to_add = *cav;
        to_add.set_lfd(this->lfd);

        if(to_add.get_power_source() >= this->power_sources.size()){
            throw SourceIndexBoundaryException(to_add.get_power_source(), this->power_sources.size());
        };

        for(vector<DiscreteOscillator>::const_iterator p_src = this->power_sources.begin(); p_src != this->power_sources.end(); p_src++) {
            to_add.add_power_source(*p_src);
        };

        for(vector<DiscreteOscillator>::const_iterator m_src = this->micro_sources.begin(); m_src != this->micro_sources.end(); m_src++) {
            to_add.add_microphonic_source(*m_src);
        };

        to_add.set_beam_loading(this->beam_loading);

        module.cavities.push_back(to_add);
    };
    
  //  omp_set_num_threads(this->cavities.size());
    return module;
};

bool CavityModuleFactory::operator==(const CavityModuleFactory& other) const {
    
    return (this->int_steps == other.int_steps);//  &&
           (this->timestep == other.timestep) &&
           (this->lfd == other.lfd) &&
           (std::equal(this->power_sources.begin(), this->power_sources.end(), other.power_sources.begin())) &&
           (std::equal(this->micro_sources.begin(), this->micro_sources.end(), other.micro_sources.begin())) &&
           (std::equal(this->cavities.begin(), this->cavities.end(), other.cavities.begin())) &&
           (this->beam_loading == other.beam_loading);
};

Json::Value CavityModuleFactory::serialize() const{
    Json::Value serialized_factory;
    serialized_factory["__cls"] = CLS_CAVITY_MODULE_FACTORY;
    serialized_factory["int_steps"] = this->int_steps;
    serialized_factory["timestep"] = this->timestep;
    Json::Value cavities(Json::arrayValue);
    for(vector<Cavity>::const_iterator cav = this->cavities.begin(); cav != this->cavities.end(); cav++){
        cavities.append((*cav).serialize());
    }

    serialized_factory["cavities"] = cavities;
    serialized_factory["lfd"] = this->lfd.serialize();

    Json::Value power_sources(Json::arrayValue);
    for(vector<DiscreteOscillator>::const_iterator p_src = this->power_sources.begin(); p_src != this->power_sources.end(); p_src++){
        power_sources.append((*p_src).serialize());
    };

    serialized_factory["power_sources"] = power_sources;
    Json::Value micro_sources(Json::arrayValue);
    for(vector<DiscreteOscillator>::const_iterator m_src = this->micro_sources.begin(); m_src != this->micro_sources.end(); m_src++){
        micro_sources.append((*m_src).serialize());
    };
    
    serialized_factory["micro_sources"] = micro_sources;
    serialized_factory["beam_loading"] = this->beam_loading.serialize();
    return serialized_factory;
};

CavityModuleFactory CavityModuleFactory::deserialize(const Json::Value& serialized_factory){
    CavityModuleFactory deserialized_factory(0.0, 0.0);
    deserialized_factory.int_steps = serialized_factory["int_steps"].asUInt();
    deserialized_factory.timestep = serialized_factory["timestep"].asDouble();
    for(Json::Value::const_iterator cav = serialized_factory["cavities"].begin();
            cav != serialized_factory["cavities"].end();
            cav++){
        deserialized_factory.cavities.push_back(Cavity::deserialize(*cav));
    }

    deserialized_factory.lfd = LorentzForceDetuning::deserialize(serialized_factory["lfd"]);

    for(Json::Value::const_iterator p_src = serialized_factory["power_sources"].begin();
            p_src != serialized_factory["power_sources"].end();
            p_src++){
        deserialized_factory.power_sources.push_back(DiscreteOscillator::deserialize(*p_src));
    }

    for(Json::Value::const_iterator m_src = serialized_factory["micro_sources"].begin();
            m_src != serialized_factory["micro_sources"].end();
            m_src++){
        deserialized_factory.micro_sources.push_back(DiscreteOscillator::deserialize(*m_src));
    }

    deserialized_factory.beam_loading = BeamLoading::deserialize(serialized_factory["beam_loading"]);
    return deserialized_factory;   
};



