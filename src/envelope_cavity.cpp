#include<cmath>
#include<functional>
#include<utility>
#include "envelope_cavity.h"
#include "boost/range/counting_range.hpp"

using namespace std;
using namespace std::placeholders;

CavitySystem::CavitySystem() : renormalize_micro_step(0) {};

void CavitySystem::deriv(const vector_type &x, vector_type &dxdt, double t) const {

    const double VI = x[0];
    const double VQ = x[1];
    const double dw = x[2];
    const double squared_V = VI * VI + VQ * VQ;
    const double dw_eff = dw - this->lfd * squared_V;

    dxdt[0] = this->RLw12 * this->II - this->w12 * VI - dw_eff * VQ;
    dxdt[1] = this->RLw12 * this->IQ - this->w12 * VQ + dw_eff * VI;
    dxdt[2] = 0.0;

    for(unsigned int i : boost::counting_range<unsigned int>(0u, this->mechanical_resonances)) {

        const unsigned int w_index = 3u + i;
        const unsigned int dw_index = 3u + i + this->mechanical_resonances; 
        const tuple<double, double, double>& resonance = this->resonance_params[i];

        dxdt[2] += x[dw_index];
        dxdt[w_index] = x[dw_index]; 
        dxdt[dw_index] = get<0>(resonance) * x[w_index] 
            + get<1>(resonance) * x[dw_index]
            + get<2>(resonance) * squared_V;
    }


    dxdt[2] += this->micro_value + (t - this->time) * this->micro_value_derivative;

};

void CavitySystem::jac(const vector_type &x, matrix_type &J, const double &t, vector_type &dfdt) const {

    const double VI = x[0];
    const double VQ = x[1];
    const double VQ_squared = VQ * VQ;
    const double VI_squared = VI * VI;
    const double dw = x[2];
    const double VIVQk2 = 2.0 * VI * VQ * this->lfd;

    J(0, 0) = VIVQk2 - this->w12;
    J(0, 1) = this->lfd * ( 3.0 * VQ_squared + VI_squared) - dw;
    J(1, 0) = dw - this->lfd * ( VQ_squared + 3.0 * VI_squared);
    J(1, 1) = -VIVQk2 - this->w12;
    J(0, 2) = -VQ;
    J(1, 2) = VI;

    for(unsigned int i : boost::counting_range<unsigned int>(0u, this->mechanical_resonances)) {

        const unsigned int w_index = 3u + i;
        const unsigned int dw_index = 3u + i + this->mechanical_resonances;  
        const tuple<double, double, double>& resonance = this->resonance_params[i];

        J(2, dw_index) = 1.0;
        J(w_index, dw_index) = 1.0;
        J(dw_index, 0) = 2.0 * get<2>(resonance) * VI; 
        J(dw_index, 1) = 2.0 * get<2>(resonance) * VQ;
        J(dw_index, w_index) = get<0>(resonance);
        J(dw_index, dw_index) = get<1>(resonance);
    }

    dfdt[2] = this->micro_value_derivative; 

};


void CavitySystem::update_micro_vals(double t) {

    this->time = t;
    this->micro_value = 0.0;
    this->micro_value_derivative = 0.0;

    for(tuple<double, double, DiscreteOscillator>& micro : this->micro_params) {

        double cos_part, sin_part;

        get<2>(micro).get_cos_sin_advance(cos_part, sin_part);
        this->micro_value += get<0>(micro) * cos_part;
        this->micro_value_derivative += get<1>(micro) * sin_part;
    }

    this->renormalize_micro_step++;
    if (this->renormalize_micro_step == STEP_BETWEEN_RENORMALIZATION) {

        this->renormalize_micro_step = 0;
        for(tuple<double, double, DiscreteOscillator>& micro : this->micro_params) {

            get<2>(micro).normalize();
        }
    }
};

EnvCavity::EnvCavity(double step_time,
        double source_frequency, 
        CavityParams cavity_params,
        double lfd, 
        BeamLoadingParams bl_params, 
        vector<MechanicalResonanceParams> mech_res_params,
        vector<MicrophonicParams> micro_params) : current_peak(IQValue(0.0, 0.0)),
    V(IQValue(0.0, 0.0)) {

        this->step_time = step_time;
        this->time = 0.0;
        this->step = 0;
        this->bl_params = bl_params;

        this->cav_sys.w12 = M_PI * cavity_params.frequency / cavity_params.Q;
        this->cav_sys.RLw12 = M_PI * cavity_params.RoQ * cavity_params.frequency;
        this->cav_sys.lfd = 2.0 * M_PI * lfd;
        this->cav_sys.mechanical_resonances = mech_res_params.size();
        this->cav_sys.system_size = 3 + 2 * mech_res_params.size();

        this->sys_status = boost::numeric::ublas::zero_vector<double>(this->cav_sys.system_size); 
        this->sys_status[0] = cavity_params.iniVI;
        this->sys_status[1] = cavity_params.iniVQ;
        this->sys_status[2] = 2.0 * M_PI * (source_frequency - cavity_params.frequency);

        unsigned int idx = 3;
        for(const MechanicalResonanceParams& resonance_params : mech_res_params) {

            const double wmo = 2.0 * M_PI * resonance_params.frequency;
            const double minus_squared_wmo = - wmo * wmo;
            const double minus_wmo_over_Q = - wmo / resonance_params.Q;
            const double minus_squared_wmo_2_mpi_ks = 2.0 * M_PI * minus_squared_wmo * resonance_params.coupling;
            this->cav_sys.resonance_params.push_back(make_tuple(minus_squared_wmo,
                        minus_wmo_over_Q,
                        minus_squared_wmo_2_mpi_ks));
            this->sys_status[idx] = resonance_params.iniw;
            this->sys_status[idx + this->cav_sys.mechanical_resonances] = resonance_params.inidw;
        }

        for(const MicrophonicParams& micro_param : micro_params) {

            const double ang_freq = micro_param.frequency;
            const double minus_micro_ang_amplitude_ang_freq = - ang_freq * 4.0 * M_PI * M_PI * micro_param.amplitude;
            const double phase = micro_param.phase;
            const double micro_ang_amplitude_squared_ang_freq = -minus_micro_ang_amplitude_ang_freq * ang_freq;
            this->cav_sys.micro_params.push_back(make_tuple(minus_micro_ang_amplitude_ang_freq,
                        micro_ang_amplitude_squared_ang_freq,
                        DiscreteOscillator(step_time, 
                            micro_param.frequency,
                            phase)));
        }


        this->current_peak = IQValue(cos(bl_params.bunch_phase) * bl_params.bunch_charge / step_time,
                sin(bl_params.bunch_phase) * bl_params.bunch_charge / step_time);
        this->start_step = (unsigned int) (bl_params.start_time/step_time);
        this->bl_params.bunch_separation++;
    };


void EnvCavity::macro_step(IQValue current){

    if((this->step >= this->start_step) and
      ((this->step % this->bl_params.bunch_separation) == 0) and
        ((this->bl_params.n_bunches < 0) or 
         (this->step - this->start_step)/this->bl_params.bunch_separation < (unsigned int)this->bl_params.n_bunches)) {
         current = this->current_peak + current;
     }

    //if ((this->step >= this->start_step) and
    //        (((this->step - this->start_step) % this->bl_params.bunch_separation + 1) == 0) and
    //        () or 
    //         ((this->step - this->start_step)/(this->bl_params.bunch_separation + 1) < (unsigned int)this->bl_params.n_bunches))){

      //  current = current + this->current_peak;
    //}

    this->cav_sys.II = current.I;
    this->cav_sys.IQ = current.Q;
    this->cav_sys.update_micro_vals(this->time);
    //        this->stepper.do_step(make_pair(bind(&CavitySystem::deriv, &this->cav_sys, _1, _2, _3 ),
    //                                        bind(&CavitySystem::jac, &this->cav_sys, _1, _2, _3, _4 )),
    //                                 this->sys_status, this->time, this->step_time);
    this->stepper.do_step(bind(&CavitySystem::deriv, &this->cav_sys, _1, _2, _3 ),
            this->sys_status, this->time, this->step_time);
    this->time += this->step_time;
    this->step++;
    this->V = IQValue(this->sys_status[0], this->sys_status[1]);
}; 


