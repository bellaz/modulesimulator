#include<omp.h>
#include "cavity_module.h"

CavityModule::CavityModule() : cavities(vector<Cavity>()) {};

vector<IQValue> CavityModule::macro_step(const vector<IQValue>& iqs){

    unsigned int i;
    const unsigned int ncav = this->cavities.size();
    vector<Cavity> *shared_cavities = &this->cavities;
    vector<IQValue> iqReturn;

    if( iqs.size() != this->cavities[0].n_power_sources()){

        throw CavityIQArgumentsException(this->cavities[0].n_power_sources(),
                                            iqs.size());
    }

//#pragma omp parallel for shared(iqs,shared_cavities) private(i)
    for(i = 0; i < ncav; i++){

        (*shared_cavities)[i].macro_step(iqs);
    };

    for(i = 0; i < ncav; i++){
        
        iqReturn.push_back(this->cavities[i].get_V());
    };

    return iqReturn; 
};

CavityModuleFactory CavityModule::to_cavity_module_factory() const {
    CavityModuleFactory factory(0.0, 0.0);
    factory.int_steps = this->cavities[0].get_int_steps();
    factory.timestep = this->cavities[0].get_timestep();
    factory.lfd = this->cavities[0].lfd;
    factory.power_sources = this->cavities[0].power_sources;
    factory.micro_sources = this->cavities[0].micro_sources;
    factory.beam_loading = this->cavities[0].beam_loading;
    factory.cavities = this->cavities;
    for(vector<Cavity>::iterator cav = factory.cavities.begin(); cav != factory.cavities.end(); cav++){
        
        (*cav).delete_lfd();
        (*cav).delete_power_sources();
        (*cav).delete_microphonic_sources();
        (*cav).delete_beam_loading();
    }

    return factory;
};

bool CavityModule::operator==(const CavityModule& other) const {
    return std::equal(this->cavities.begin(), this->cavities.end(), other.cavities.begin());
};

Json::Value CavityModule::serialize() const {

    Json::Value serialized_module;
    serialized_module["__cls"] = CLS_CAVITY_MODULE;
    Json::Value serialized_cavities(Json::arrayValue); 
    for(vector<Cavity>::const_iterator it = this->cavities.begin(); it != this->cavities.end(); it++) {
        serialized_cavities.append((*it).serialize());
    }

    serialized_module["cavities"] = serialized_cavities;
    
    return serialized_module;
};

CavityModule CavityModule::deserialize(const Json::Value& serialized_module){
    
    CavityModule deserialized_module;
    for(const Json::Value& ser_mod : serialized_module["cavities"]) {
        deserialized_module.cavities.push_back(Cavity::deserialize(ser_mod));
    }
    
    return deserialized_module;
};
