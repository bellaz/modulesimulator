/*
 * Cavity with errors and sources eg - LFD, BL, Microphonics, Multiple Generators
 * It computes a macro timestep where slow effects are assumed as constant
 */

#pragma once

#include<vector>
#include <jsoncpp/json/json.h>
#include <cmath>
#include "ideal_cavity.h"
#include "discrete_oscillator.h"
#include "iq_value.h"
#include "lfd.h"
#include "beam_loading.h"

#define _USE_MATH_DEFINES
#define CLS_CAVITY "Cavity"

class CavityModule;

using namespace std;

class Cavity {

    friend class CavityModule;

    public:

    Cavity(unsigned int int_step = 1, double timestep = 1.0, double frequency = 1.0, double Q = 100.0, double RoQ = 1.0, IQValue initial_state = IQValue(0.0, 0.0));

    void add_power_source(const DiscreteOscillator& power_source); // these MUST have amplitude = 1

    void delete_power_sources();

    void set_lfd(const LorentzForceDetuning& lfd);

    void delete_lfd();

    void set_beam_loading(const BeamLoading& bl);
    
    void delete_beam_loading();

    void add_microphonic_source(const DiscreteOscillator& micro_source);

    void delete_microphonic_sources();

    void set_power_source(unsigned int ref_power_source);

    unsigned int get_power_source() const;

    unsigned int get_n_sources() const;
   
    double get_resonant_frequency() const;

    double get_timestep() const;

    unsigned int get_int_steps() const;

    IQValue get_V() const {

        return this->V;
    }

    double get_mean_V() const {
        
        return this->mean_V;
    }

    // most important function. It has to be executed REALLY fast
    void macro_step(const vector<IQValue>& power_sources_IQ){

        double delta_frequency = 0;
        double current = 0.0;

        IQValue V(0.0, 0.0);
        double mean_V = 0.0;

        for(vector<DiscreteOscillator>::iterator it = this->micro_sources.begin();
            it != this->micro_sources.end();
            it++){

            delta_frequency += (*it).get_sin();
            (*it).advance();
        }

        this->lfd.step(this->mean_V * 0.5 * M_PI);
        delta_frequency += this->lfd.get_frequency_delta();
        
        this->ideal_cavity.recalculate_coefficients(delta_frequency);
         
        for(vector<DiscreteOscillator>::iterator power_source = this->power_sources.begin();
                power_source != this->power_sources.end();
                power_source++){

                (*power_source).back_step();
            }



        for(unsigned int step = this->int_steps; step != 0; step--){ 

            current = 0.0;
            vector<IQValue>::const_iterator power_source_IQ = power_sources_IQ.begin();
            vector<DiscreteOscillator>::iterator power_source = this->power_sources.begin();

            for(; power_source_IQ != power_sources_IQ.end(); power_source_IQ++){
           
                (*power_source).advance();
                current += (*power_source).get_cos_sin_as_IQ().scalar((*power_source_IQ));
                power_source++;
            }
            
            this->ideal_cavity.step(current);
            V += this->power_sources[this->ref_power_source].get_cos_sin_as_IQ() *
                 this->ideal_cavity.get_actual_V();
            
            mean_V += abs(this->ideal_cavity.get_actual_V());
            
            }

        for(vector<DiscreteOscillator>::iterator power_source = this->power_sources.begin();
                power_source != this->power_sources.end();
                power_source++){

                (*power_source).advance();
                (*power_source).normalize();
            }


        V *= (2.0/(double)this->int_steps);
        this->V = V;
        mean_V /= (double) this->int_steps;
        this->mean_V = mean_V;
    }

    bool operator==(const Cavity& other) const;

    unsigned int n_power_sources(){
        
        return this->power_sources.size();
    };

    Json::Value serialize() const;
    static Cavity deserialize(const Json::Value& serialized_cavity);

    private:

    unsigned int int_steps;
    IdealCavity ideal_cavity;
    vector<DiscreteOscillator> power_sources;
    unsigned int ref_power_source;
    LorentzForceDetuning lfd;
    BeamLoading beam_loading;
    vector<DiscreteOscillator> micro_sources;

    IQValue V;
    double mean_V;
};
