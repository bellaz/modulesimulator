/*
 * This module provide Beam Loading class 
 */

#pragma once
#include<cmath>
#include<jsoncpp/json/json.h>

using namespace std;

//hack to make things faster
inline double mfmod(double x,double y) { 
    double a=x/y; 
    return (a-(int)a)*y; 
}

class BeamLoading{
    public:
        BeamLoading(double timestep = 1.0, double baseband_frequency = 1.0, double bunch_charge =0.0, unsigned int bunch_separation = 0, double bunch_phase = 0.0, double start_time = 0.0, int n_bunches = 0, double baseband_phase = 0.0);

        void step(){
            
            this->time += timestep;
            double start_elapsed_time = this->time - this->real_start;
            this->out_current = 0.0;

            if((this->bunches_to_do != 0) and (start_elapsed_time >= 0.0)){
                

                if(mfmod(start_elapsed_time, this->time_separation) < this->timestep){
                    
                    this->bunches_to_do--;
                    this->out_current = this->step_current;
                }

            }
            
        };

        double get_current() const {
        
            return this->out_current;
        }

        bool operator==(const BeamLoading& other) const;
        void reset();
        Json::Value serialize() const;
        static BeamLoading deserialize(const Json::Value&);

    private:

        double timestep;
        double baseband_frequency;
        double bunch_charge;
        unsigned int bunch_separation;
        double bunch_phase;
        double start_time;
        int n_bunches;
        double baseband_phase;
        double out_current;
        double step_current;
        double time_separation;
        double time;
        double real_start;
        int bunches_to_do;
};

