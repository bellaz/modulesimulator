/*
 * Module that provides a discrete oscillator. Each iteration
 * produces a fixed angle movement. Usable in high performance section code
 */

#pragma once
#include<jsoncpp/json/json.h>
#include "iq_value.h"

#define CLS_DISCRETE_OSCILLATOR "DiscreteOscillator"

class DiscreteOscillator {

    public:
    DiscreteOscillator(double timestep=1.0, double frequency=0.0, double phase_0 = 0.0, double amplitude = 1.0);
    
    void advance(){
        double cos_part_old = this->cos_part;
        double sin_part_old = this->sin_part;
        this->cos_part = cos_part_old * this->diag_el     - sin_part_old * this->out_diag_el;
        this->sin_part = cos_part_old * this->out_diag_el + sin_part_old * this->diag_el;
    };

    void back_step(){

        double cos_part_old = this->cos_part;
        double sin_part_old = this->sin_part;
        this->cos_part =  cos_part_old * this->diag_el     + sin_part_old * this->out_diag_el;
        this->sin_part = -cos_part_old * this->out_diag_el + sin_part_old * this->diag_el;
        this->cos_part /= this->diag_el * this->diag_el + this->out_diag_el * this->out_diag_el;
        this->sin_part /= this->diag_el * this->diag_el + this->out_diag_el * this->out_diag_el;
    };

    void get_cos_sin(double& cosp, double& sinp) const {
    cosp = this->cos_part;
    sinp = this->sin_part;
    };
    void get_cos_sin_advance(double& cosp, double& sinp){
        this->get_cos_sin(cosp,sinp);
        this->advance();
    };
    double get_cos() const {
        return this->cos_part;
    };
    double get_sin() const {
        return this->sin_part;
    }; 
    double get_cos_advance(){
        this->advance();
        return this->cos_part;
    }
    double get_sin_advance(){
        this->advance();
        return this->sin_part;
    }

    double get_angle() const;

    void add_angle(double angle);

    void set_angle(double angle);

    double get_angle_as_time() const;

    void set_angle_as_time(double time);

    void add_angle_as_time(double time);

    IQValue get_cos_sin_as_IQ() const{
        return IQValue(this->cos_part, this->sin_part);
    }

    double get_amplitude() const;

    void set_amplitude(double amp);

    void normalize();

    bool operator==(const DiscreteOscillator& other) const;

    Json::Value serialize() const;
    static DiscreteOscillator deserialize(const Json::Value&);

    private:

    double cos_part, sin_part;
    double diag_el, out_diag_el;
    double frequency, timestep, phase_0;
    double amplitude;
};
