#include<omp.h>
#include "envelope_cavity_module.h"

EnvCavityModule::EnvCavityModule(double step_time,
            double source_frequency, 
            std::vector<CavityParams> cavities_params,
            double lfd, 
            BeamLoadingParams bl_params, 
            std::vector<MechanicalResonanceParams> mech_res_params,
            std::vector<MicrophonicParams> micro_params) {

    for(const CavityParams& cavity_params : cavities_params) {
        
        this->cavities.push_back(EnvCavity(step_time,
                                           source_frequency,
                                           cavity_params,
                                           lfd,
                                           bl_params,
                                           mech_res_params,
                                           micro_params));
    }

//    omp_set_num_threads(this->cavities.size());
};

vector<IQValue> EnvCavityModule::macro_step(IQValue current) {

    unsigned int i;
    const unsigned int ncav = this->cavities.size();
    vector<EnvCavity> *shared_cavities = &this->cavities;
    vector<IQValue> iqReturn;

//#pragma omp parallel for shared(current, shared_cavities) private(i)
    for(i = 0; i < ncav; i++){

        (*shared_cavities)[i].macro_step(current);
    };

    for(i = 0; i < ncav; i++){
        
        iqReturn.push_back(this->cavities[i].get_V());
    };

    return iqReturn; 
};
