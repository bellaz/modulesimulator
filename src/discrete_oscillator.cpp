#include "discrete_oscillator.h"
#include<cmath>
#include<string>

#define _USE_MATH_DEFINES

using namespace std;

DiscreteOscillator::DiscreteOscillator(double timestep, double frequency, double phase_0, double amplitude) : 
frequency(frequency), timestep(timestep), phase_0(phase_0), amplitude(amplitude) {
    double angle_per_step = frequency * timestep * 2.0 * M_PI;
    this->cos_part    = cos(phase_0) * this->amplitude;
    this->sin_part    = sin(phase_0) * this->amplitude;
    this->diag_el     = cos(angle_per_step);
    this->out_diag_el = sin(angle_per_step);
};

double DiscreteOscillator::get_angle() const {
    
    return atan2(this->sin_part, this->cos_part);   
};

void DiscreteOscillator::add_angle(double angle){
    this->set_angle(this->get_angle() + angle);
};

void DiscreteOscillator::set_angle(double angle){
    this->cos_part    = cos(angle);
    this->sin_part    = sin(angle);
};

double DiscreteOscillator::get_angle_as_time() const {
    return this->get_angle() / (2.0 * M_PI * this->frequency);
};

void DiscreteOscillator::set_angle_as_time(double time){
    this->set_angle(2.0 * M_PI * time * this->frequency); 
};

void DiscreteOscillator::add_angle_as_time(double time){
    this->set_angle_as_time(time + this->get_angle_as_time());    
};

void DiscreteOscillator::normalize(){

    double norm = sqrt(this->cos_part * this->cos_part + this->sin_part * this->sin_part);
    if (norm > 0.0){
        this->cos_part *= (this->amplitude/norm);
        this->sin_part *= (this->amplitude/norm);
    }
};

double DiscreteOscillator::get_amplitude() const {

    return this->amplitude;
};

void DiscreteOscillator::set_amplitude(double amp){

    this->amplitude = amp;
    this->normalize();
};

bool DiscreteOscillator::operator==(const DiscreteOscillator& other) const {
    
    return (this->cos_part == other.cos_part) &&
           (this->sin_part == other.sin_part) &&
           (this->diag_el  == other.diag_el) &&
           (this->out_diag_el == other.out_diag_el) &&
           (this->frequency == other.frequency) &&
           (this->timestep == other.timestep) &&
           (this->phase_0 == other.phase_0) &&
           (this->amplitude == other.amplitude);

}

Json::Value DiscreteOscillator::serialize() const {

    Json::Value serialized_osc;
    serialized_osc["__cls"]     = string(CLS_DISCRETE_OSCILLATOR);     
    serialized_osc["cos_part"]  = this->cos_part;
    serialized_osc["sin_part"]  = this->sin_part;
    serialized_osc["diag_el"]   = this->diag_el;
    serialized_osc["out_diag_el"] = this->out_diag_el;
    serialized_osc["frequency"] = this->frequency;
    serialized_osc["timestep"]  = this->timestep;
    serialized_osc["phase_0"]   = this->phase_0;
    serialized_osc["amplitude"]  = this->amplitude;

    return serialized_osc;
};


DiscreteOscillator DiscreteOscillator::deserialize(const Json::Value& serialized_osc){

    DiscreteOscillator deser_osc;

    deser_osc.cos_part = serialized_osc["cos_part"].asDouble();
    deser_osc.sin_part = serialized_osc["sin_part"].asDouble();
    deser_osc.diag_el  = serialized_osc["diag_el"].asDouble();
    deser_osc.out_diag_el = serialized_osc["out_diag_el"].asDouble();
    deser_osc.frequency = serialized_osc["frequency"].asDouble();  
    deser_osc.timestep  = serialized_osc["timestep"].asDouble();
    deser_osc.phase_0   = serialized_osc["phase_0"].asDouble();   
    deser_osc.amplitude  = serialized_osc["amplitude"].asDouble();
    return deser_osc;    
};


