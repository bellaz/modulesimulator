// This module provides a wrap for boost python of cavity module

#pragma once
#include<boost/python.hpp>
#include "cavity_module.h"

class WrapCavityModule;

#include "wrap_cavity_module_factory.h"

using namespace boost::python;

class WrapCavityModule : public CavityModule {
    public:
        WrapCavityModule(CavityModule& cavity_module); 
        list wrapped_macro_step(const list& iq_list);
        str wrapped_serialize() const;
        static WrapCavityModule wrapped_deserialize(const str& serialized_module);
        WrapCavityModuleFactory wrapped_to_factory() const;
};
