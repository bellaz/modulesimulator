#include<string>
#include<algorithm>
#include "lfd.h"


using namespace std;

LorentzForceDetuning::LorentzForceDetuning(double timestep,  double k_lfd) :
    timestep(timestep), k_lfd(k_lfd), delta_frequency(0.0), V0(0.0), Vm1(0.0), 
    mechanical_resonances(std::vector<MechanicalResonance>()) {};

void LorentzForceDetuning::step(double V1){
        // a*x^2 + b*x + c = V_k where x is the step. for Vm1 -> -1, V0 -> 0, V1 ->1, c = V0
        double a = 0.5 * (V1 + this->Vm1) - this->V0; 
        double b = 0.5 * (V1 - this->Vm1);
        double c = this->V0;
        double V_1p5 = a * 2.25 + b * 1.5 + c;
        this->Vm1 = this->V0;
        this->V0 = V1;
        this->delta_frequency = this->k_lfd * V_1p5 * V_1p5;
        for(MechanicalResonance& resonance : this->mechanical_resonances) {

            resonance.step(V_1p5 * V_1p5);
            this->delta_frequency += resonance.get_actual_Vint();
        }
};

double LorentzForceDetuning::get_frequency_delta() const {

    return this->delta_frequency;
};

double LorentzForceDetuning::get_k_lfd() const {
    return this->k_lfd;
};

void LorentzForceDetuning::set_k_lfd(double k_lfd){
    this->k_lfd = k_lfd;
};

void LorentzForceDetuning::add_mechanical_resonance(double frequency, double Q, double coupling){

    this->mechanical_resonances.push_back(MechanicalResonance(this->timestep, frequency, Q, coupling * 2.0 * M_PI * frequency));
};

void LorentzForceDetuning::delete_mechanical_resonances() {

    this->mechanical_resonances.clear();
}

bool LorentzForceDetuning::operator==(const LorentzForceDetuning& other) const {
    
    return (this->timestep == other.timestep) &&
           (this->k_lfd == other.k_lfd) &&
           (this->delta_frequency == other.delta_frequency) &&
           (this->V0 == other.V0) &&
           (this->Vm1 == other.Vm1) &&
           (this->k_lfd == other.k_lfd) &&
           (std::equal(this->mechanical_resonances.begin(),
                       this->mechanical_resonances.end(),
                       other.mechanical_resonances.begin()));
};

Json::Value LorentzForceDetuning::serialize() const {

    Json::Value serialized_lfd;
    serialized_lfd["__cls"]           = string("LorentzForceDetuning");
    serialized_lfd["timestep"]        = this->timestep;
    serialized_lfd["k_lfd"]           = this->k_lfd;
    serialized_lfd["delta_frequency"] = this->delta_frequency;
    serialized_lfd["V0"]              = this->V0;
    serialized_lfd["Vm1"]             = this->Vm1;
    Json::Value serialized_mec_resonances(Json::arrayValue);
    for (const MechanicalResonance& resonance : this->mechanical_resonances){
        
        serialized_mec_resonances.append(resonance.serialize());
    }

    serialized_lfd["mechanical_resonances"] = serialized_mec_resonances;

    return serialized_lfd;
}

LorentzForceDetuning LorentzForceDetuning::deserialize(const Json::Value& serialized_lfd){
    
    LorentzForceDetuning deserialized_lfd;

    deserialized_lfd.timestep         =      serialized_lfd["timestep"].asDouble();         
    deserialized_lfd.k_lfd            =      serialized_lfd["k_lfd"].asDouble();            
    deserialized_lfd.delta_frequency  =      serialized_lfd["delta_frequency"].asDouble();  
    deserialized_lfd.V0               =      serialized_lfd["V0"].asDouble();               
    deserialized_lfd.Vm1              =      serialized_lfd["Vm1"].asDouble();              

    for(Json::ValueConstIterator resonance = serialized_lfd["mechanical_resonances"].begin();
        resonance != serialized_lfd["mechanical_resonances"].end();
        resonance++) {
        
        deserialized_lfd.mechanical_resonances.push_back(MechanicalResonance::deserialize(*resonance));
    }

    return deserialized_lfd;
}
