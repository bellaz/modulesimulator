#include<cmath>
#include<string>
#include "ideal_cavity.h"

#define _USE_MATH_DEFINES
#define CLS_IDEAL_CAVITY "IdealCavity"

using namespace std;

IdealCavity::IdealCavity(double timestep, double frequency, double Q, double RoQ, IQValue initial_state) :
    timestep(timestep), frequency(frequency), Q(Q), RoQ(RoQ) {
        this->V    = initial_state.I;
        this->VPast = initial_state.I;
        this->Vint = initial_state.Q;
        this->VintPast = initial_state.Q;
        this->recalculate_coefficients();
    };

void IdealCavity::recalculate_coefficients(double delta_frequency, double delta_Q, double delta_RoQ){

    this->actual_frequency = this->frequency + delta_frequency;
    this->actual_Q = this->Q + delta_Q;
    this->actual_RoQ = this->RoQ + delta_RoQ;

    double timestep = this->timestep;
    double ang_freq = 2.0 * M_PI * this->actual_frequency;
    double a = ang_freq/this->Q;
    double b = ang_freq * ang_freq;
    double z = 2.0 * ang_freq * this->RoQ;
    timestep *= 0.5;
    double squared = sqrt(4*b - a*a);
    double matexp = exp(-a * timestep);
    double matcos = matexp * cos(squared * timestep);
    double matsin = matexp * sin(squared * timestep)/squared;

    this->m00 = (matcos + a * matsin);
    this->m01 = 2.0 * matsin;
    this->m10 = -2.0 * b * matsin;
    this->m11 = (matcos - a * matsin);
    this->mz1 = -z * 0.5/b;
    this->mz2 = this->mz1 * this->m10;
    this->mz1 *= (this->m00 - 1.0);
};

double IdealCavity::get_angle(double driving_freq) const {

    double VMean = 0.5 * (this->V + this->VPast);
    double VintMean = 0.5 * (this->Vint + this-> VintPast);
    return atan2( VintMean * (2.0 * M_PI * driving_freq), VMean);   
};

void IdealCavity::add_angle(double driving_freq, double angle){

    this->set_angle(driving_freq, this->get_angle(driving_freq) + angle);
};

void IdealCavity::set_angle(double driving_freq, double angle){

    double VMean = 0.5 * (this->V + this->VPast);
    double VintMean = 0.5 * (this->Vint + this-> VintPast);
    double V_amp = sqrt(VMean * VMean + VintMean * VintMean *( 4.0 * M_PI * M_PI * driving_freq * driving_freq));
    double angle_step = this->timestep * driving_freq * M_PI;
    this->V    = V_amp * cos(angle + angle_step);
    this->Vint = V_amp * sin(angle + angle_step) / (2.0 * M_PI * driving_freq);
    this->VPast = V_amp * cos(angle - angle_step);
    this->VintPast = V_amp * sin(angle - angle_step) / (2.0 * M_PI * driving_freq);

};

double IdealCavity::get_angle_as_time(double driving_freq) const {

    return this->get_angle(driving_freq) / (2.0 * M_PI  * driving_freq);
};

void IdealCavity::set_angle_as_time(double driving_freq, double time){

    this->set_angle(driving_freq, 2.0 * M_PI * time * driving_freq); 
};

void IdealCavity::add_angle_as_time(double driving_freq, double time){

    this->set_angle_as_time(driving_freq, time + this->get_angle_as_time(driving_freq));    
};

double IdealCavity::get_timestep() const {
    return this->timestep;
};

bool IdealCavity::operator==(const IdealCavity& other) const {

    bool retval = (this->V == other.V)
        && (this->Vint == other.Vint)
        && (this->timestep == other.timestep)
        && (this->frequency == other.frequency)
        && (this->Q == other.Q)
        && (this->RoQ == other.RoQ)
        && (this->actual_frequency == other.actual_frequency)
        && (this->actual_Q == other.actual_Q)
        && (this->actual_RoQ == other.actual_RoQ)
        && (this->m00 == other.m00)
        && (this->m10 == other.m10)
        && (this->m01 == other.m01)
        && (this->m11 == other.m11)
        && (this->mz1 == other.mz1)
        && (this->mz2 == other.mz2);

    return retval;
}



Json::Value IdealCavity::serialize() const {
    Json::Value serialized_cavity;
    serialized_cavity["__cls"]            = string(CLS_IDEAL_CAVITY);
    serialized_cavity["V"]                = this->V;
    serialized_cavity["Vint"]             = this->Vint;
    serialized_cavity["VPast"]            = this->VPast;
    serialized_cavity["VintPast"]         = this->VintPast;
    serialized_cavity["timestep"]         = this->timestep;
    serialized_cavity["frequency"]        = this->frequency;
    serialized_cavity["Q"]                = this->Q;
    serialized_cavity["RoQ"]              = this->RoQ;
    serialized_cavity["actual_frequency"] = this->actual_frequency;
    serialized_cavity["actual_Q"]         = this->actual_Q;
    serialized_cavity["actual_RoQ"]       = this->actual_RoQ;
    serialized_cavity["m00"]              = this->m00;
    serialized_cavity["m01"]              = this->m01;
    serialized_cavity["m10"]              = this->m10;
    serialized_cavity["m11"]              = this->m11;
    serialized_cavity["mz1"]              = this->mz1;
    serialized_cavity["mz2"]              = this->mz2;

    return serialized_cavity;
};

IdealCavity IdealCavity::deserialize(const Json::Value& serialized_cavity){
    IdealCavity deserialized_cavity(1.0, 1.0, 1.0, 1.0);
    deserialized_cavity.V                = serialized_cavity["V"].asDouble();                
    deserialized_cavity.Vint             = serialized_cavity["Vint"].asDouble();             
    deserialized_cavity.VPast            = serialized_cavity["VPast"].asDouble();
    deserialized_cavity.VintPast         = serialized_cavity["VintPast"].asDouble();
    deserialized_cavity.timestep         = serialized_cavity["timestep"].asDouble();         
    deserialized_cavity.frequency        = serialized_cavity["frequency"].asDouble();        
    deserialized_cavity.Q                = serialized_cavity["Q"].asDouble();                
    deserialized_cavity.RoQ              = serialized_cavity["RoQ"].asDouble();              
    deserialized_cavity.actual_frequency = serialized_cavity["actual_frequency"].asDouble(); 
    deserialized_cavity.actual_Q         = serialized_cavity["actual_Q"].asDouble();         
    deserialized_cavity.actual_RoQ       = serialized_cavity["actual_RoQ"].asDouble();       
    deserialized_cavity.m00              = serialized_cavity["m00"].asDouble();                  
    deserialized_cavity.m01              = serialized_cavity["m01"].asDouble(); 
    deserialized_cavity.m10              = serialized_cavity["m10"].asDouble(); 
    deserialized_cavity.m11              = serialized_cavity["m11"].asDouble(); 
    deserialized_cavity.mz1              = serialized_cavity["mz1"].asDouble(); 
    deserialized_cavity.mz2              = serialized_cavity["mz2"].asDouble(); 

    return deserialized_cavity;
};

