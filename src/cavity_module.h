/*
 *
 * This represents a cavity module that keeps runnable cavities. It is exposed 
 * to python through its interface
 */

#pragma once

#include<vector>
#include "module_basic_exception.h"
#include "cavity_module_factory.h"
#include "cavity.h"
#include "iq_value.h"

#define CLS_CAVITY_MODULE "CavityModule"

using namespace std;

class CavityModuleFactory;

class CavityModule{

    friend class CavityModuleFactory;

    public:

    vector<IQValue> macro_step(const vector<IQValue>& iqs);

    CavityModuleFactory to_cavity_module_factory() const;

    bool operator==(const CavityModule& other) const;
    Json::Value serialize() const;
    static CavityModule deserialize(const Json::Value& serialized_cavity);

    private:

    CavityModule();
    vector<Cavity> cavities;
};

class CavityIQArgumentsException : ModuleException {

    public:
        CavityIQArgumentsException(unsigned int expected, unsigned int passed) :
            expected(expected), passed(passed) {};
        char const* what() throw() {

            string err_str;
            err_str += "number of expected iq_values: ";
            err_str += this->expected;
            err_str += " .number of passed iq_values: ";
            err_str += passed;
            return err_str.c_str();
        };

    private:

        int expected, passed;
};

