#include "wrap_cavity_module.h" 

WrapCavityModule::WrapCavityModule(CavityModule& cavity_module) : CavityModule(cavity_module) {}; 

list WrapCavityModule::wrapped_macro_step(const list& iq_list) {

    vector<IQValue> iq_list_;
    vector<IQValue> iq_return_;
    list iq_return;

    for(auto iq_it = stl_input_iterator<boost::python::object>(iq_list);
        iq_it != stl_input_iterator<boost::python::object>();
        iq_it++) {
        iq_list_.push_back(IQValue(extract<double>((*iq_it)[0]),
                                   extract<double>((*iq_it)[1])));
    }

    iq_return_ = this->CavityModule::macro_step(iq_list_);

    for(const IQValue& iq_ret_value : iq_return_){
        
        boost::python::tuple iq_ret_value_ = boost::python::make_tuple<double, double>(iq_ret_value.I,
                                                                                       iq_ret_value.Q);
        iq_return.append(iq_ret_value_);
    }

    return iq_return;
};

str WrapCavityModule::wrapped_serialize() const {

    Json::Value serialized_json = this->CavityModule::serialize();
    Json::StyledWriter writer;
    return str(writer.write(serialized_json));
};

WrapCavityModule WrapCavityModule::wrapped_deserialize(const str& serialized_module) {
    Json::Reader reader;
    Json::Value  parsed;
    const char* raw_serialized = extract<const char*>(serialized_module);
    std::string serialized_module_(raw_serialized);
    reader.parse(serialized_module_, parsed);
    CavityModule cavity_module = CavityModule::deserialize(parsed);
    return WrapCavityModule(cavity_module);
};

WrapCavityModuleFactory WrapCavityModule::wrapped_to_factory() const {

    CavityModuleFactory factory = this->CavityModule::to_cavity_module_factory(); 
    return WrapCavityModuleFactory(factory);
};
