#include<vector>
#include<tuple>
#include <boost/numeric/odeint.hpp>
//#include <boost/numeric/odeint/stepper/rosenbrock4.hpp>
#include <boost/numeric/odeint/stepper/runge_kutta4.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include "envelope_native_types.h"
#include "iq_value.h"
#include "discrete_oscillator.h"

#define STEP_BETWEEN_RENORMALIZATION 1000

using namespace boost::numeric::odeint;

typedef boost::numeric::ublas::vector<double> vector_type;
typedef boost::numeric::ublas::matrix<double> matrix_type;



typedef boost::numeric::odeint::runge_kutta4<vector_type> stepper_type;
//typedef boost::numeric::odeint::rosenbrock4<double> stepper_type;

class EnvCavity;

class CavitySystem {

    friend class EnvCavity;
    public:

    void deriv(const vector_type &x , vector_type &dxdt , double t) const;
    void jac(const vector_type &x , matrix_type &J ,const double &t , vector_type &dfdt ) const; 
    void update_micro_vals(double t);

    private:

    CavitySystem();

    double w12;
    double RLw12;
    double lfd;
    //std::vector<double> minus_micro_ang_amplitudes_ang_freqs;
    //std::vector<double> micro_ang_freqs;
    //std::vector<double> micro_phase;
    //std::vector<double> minus_micro_ang_amplitudes_squared_ang_freqs;
    std::vector<std::tuple<double, double, DiscreteOscillator>> micro_params; // the two datas commented on top 


    double micro_value;
    double micro_value_derivative;

    //std::vector<double> minus_squard_wmos;
    //std::vector<double> minus_wmo_over_Q;
    //std::vector<double> minus_squared_wmos_2_mpi_ks;    
    std::vector<std::tuple<double, double, double>> resonance_params; // the three datas commented on top

    unsigned renormalize_micro_step; 

    vector_type sys_status;
    double II, IQ;
    double time;

    unsigned int mechanical_resonances;
    unsigned int system_size;
};


class DummyCavitySysJac {

    public:
    DummyCavitySysJac(CavitySystem *cav_sys);
    void operator()(const vector_type &x , matrix_type &J, const double &t, vector_type &dfdt ) const;
    private:
    CavitySystem *cav_sys;

};

class EnvCavity {

    public:
    EnvCavity(double step_time,
            double source_frequency, 
            CavityParams cavity_params,
            double lfd, 
            BeamLoadingParams bl_params, 
            std::vector<MechanicalResonanceParams> mech_res_params,
            std::vector<MicrophonicParams> micro_params);

    void macro_step(IQValue current); 
    IQValue get_V() const {return this->V;}; 

    private: 
    IQValue current_peak;
    unsigned int step;
    BeamLoadingParams bl_params;
    double step_time;
    double time;
    unsigned int start_step;
    vector_type sys_status;
    stepper_type stepper;
    CavitySystem cav_sys;
    IQValue V;
};
