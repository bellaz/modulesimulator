/*
 * This module implement an ideal accelerating cavities modeled as a simple
 * resonant circuit
 *
 */
#pragma once
#include<cmath>
#include<jsoncpp/json/json.h>
#include "iq_value.h"
// Ideal cavity module. without considering all disturbance effects
class IdealCavity{
    public: 
    IdealCavity(double timestep=1.0, double frequency=1.0, double Q=100.0, double RoQ=1.0, IQValue initial_state = IQValue(0.0,0.0));
    
    void step(double current){
        this->VPast = this->V;
        this->VintPast = this->Vint;
        this->V = this->m10 * this->VintPast + this->m11 * this->VPast + this->mz2 * current;
        this->Vint = this->m00 * this->VintPast + this->m01 * this->VPast + this->mz1 * current;
    };

    void recalculate_coefficients(double delta_frequency = 0.0, double delta_Q = 0.0, double delta_RoQ = 0.0);
    double get_actual_V() const {
        return (this->V + this->VPast) * 0.5;   
    };

    double get_actual_Vint() const {
        return (this->Vint + this->VintPast) * 0.5;
    }

    double get_actual_resonant_frequency() const {
        return this->actual_frequency;   
    };
    double get_actual_Q() const {
        return this->actual_Q;
    };
    double get_actual_RoQ() const {
        return this->actual_RoQ;   
    };

    double get_resonant_frequency() const {
        return this->frequency;   
    };
    double get_Q() const {
        return this->Q;
    };
    double get_RoQ() const {
        return this->RoQ;   
    };

    double get_angle(double driving_freq) const;

    void add_angle(double driving_freq, double angle);

    void set_angle(double driving_freq, double angle);

    double get_angle_as_time(double driving_freq) const;

    void set_angle_as_time(double driving_freq, double time);

    void add_angle_as_time(double driving_freq, double time);

    double get_timestep() const;

    bool operator==(const IdealCavity& other) const;

    Json::Value serialize() const;
    static IdealCavity deserialize(const Json::Value& serialized_cavity);

    private:
    double V;
    double Vint;
    double VPast;
    double VintPast;
    double timestep;
    double frequency;
    double Q;
    double RoQ;
    double actual_frequency;
    double actual_Q;
    double actual_RoQ;
    double m00, m10, m01, m11, mz1, mz2;
};
