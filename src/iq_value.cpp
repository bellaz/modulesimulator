#include<string>
#include "iq_value.h"

Json::Value IQValue::serialize() const {
    Json::Value serialized_iq_value;
    serialized_iq_value["__cls"] = string(CLS_IQ_VALUE);
    serialized_iq_value["I"] = this->I;
    serialized_iq_value["Q"] = this->Q;
    
    return serialized_iq_value;
};

IQValue IQValue::deserialize(const Json::Value& serialized_iq_value){
    IQValue deserialized_iq_value(0.0, 0.0);
    deserialized_iq_value.I = serialized_iq_value["I"].asDouble();
    deserialized_iq_value.Q = serialized_iq_value["Q"].asDouble();

    return deserialized_iq_value;
};


